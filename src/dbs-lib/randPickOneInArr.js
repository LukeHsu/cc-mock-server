const _ = require('lodash')

module.exports = arr => _.get(arr, _.random(arr.length-1))
