module.exports = {
  convert: str => Buffer.from(str, 'base64').toString('utf8')
}
