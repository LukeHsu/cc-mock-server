const random = require('lodash/random')
const get = require('lodash/get')

module.exports = {
  get: funcs => get(funcs, random(funcs.length - 1))()
}
