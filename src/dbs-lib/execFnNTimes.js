const _ = require('lodash')

module.exports = (times = 1, fn) => _.map(_.range(times), () => fn())