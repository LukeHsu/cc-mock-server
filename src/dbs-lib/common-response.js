const moment = require('moment')
const faker = require('faker')
// 數字前面補 0
function prefixInteger (num, length) {
  return (Array(length).join('0') + num).slice(-length)
}

function randomTxSN () {
  return moment().format('HHmmss') + prefixInteger(faker.finance.amount(0, 99999999, 0), 8)
}

const response = {
  header: {
    txSN: randomTxSN(), // String 交易序號，格式為HHmmss+8碼亂數
    txDate: moment().utc().format('YYYYMMDDHHmmss'), // 交易時間 yyyyMMddHHmmss(UTC)
    txID: '', // String 交易代碼(eg. CCD010203)
    channel: 'CCDTW', // String 通道代碼 CCDS Web：CCDTW , CCDS APP：CCDTM
    lang: 'zh-TW', // String 使用者語言 zh-TW ; en-US
    sessionID: '',
    returnCode: 'M0000', // String API 回覆代碼0000 – 成功, other – 其他
    returnMessage: 'Success'
  },
  body: {}
}

function createResponse (txID) {
  const newOne = JSON.parse(JSON.stringify(response))
  if (txID) {
    newOne.header.txID = txID
  }
  return newOne
}

module.exports.createResponse = createResponse
