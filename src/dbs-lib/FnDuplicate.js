const map = require('lodash/map')
const range = require('lodash/range')

module.exports = {
  map: (times = 1, fn) => map(range(times), () => fn())
}
