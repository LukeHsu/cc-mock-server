const random = require('lodash/random')
const get = require('lodash/get')

module.exports = {
  get: arr => get(arr, random(arr.length - 1))
}
