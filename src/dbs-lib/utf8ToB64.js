module.exports = {
  convert: str => Buffer.from(str).toString('base64')
}
