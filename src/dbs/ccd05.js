const express = require('express')
const faker = require('faker')
const moment = require('moment')
const _ = require('lodash')

const createResponse = require('../dbs-lib/common-response').createResponse
const randPickOneInArr = require('../dbs-lib/randPickOneInArr')
const execFnNTimes = require('../dbs-lib/execFnNTimes')

const router = express.Router()

const allCardList = [{
  'cardName': '星展威士普卡',
  'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/dbs/DBS_Visa_Classic_logo100.jpg',
  'cardProdID': '100'
}, {
  'cardName': '星展everyday白金卡',
  'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/anz/Shop&Dine_Classic_Visa_Logo151.jpg',
  'cardProdID': '151'
}, {
  'cardName': '星展飛行鈦金卡你馬(亦垮通)',
  'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/anz/Travel_Platinum_MC_Logo361,362.jpg',
  'cardProdID': '362'
}]

const allCardList2 = [{
  'cardName': '星展威士普卡',
  'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/dbs/DBS_Visa_Classic_logo100.jpg',
  'cardProdID': '100'
}, {
  'cardName': '星展everyday白金卡',
  'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/anz/Shop&Dine_Classic_Visa_Logo151.jpg',
  'cardProdID': '151'
}, {
  'cardName': '星展飛行鈦金卡',
  'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/anz/Travel_Platinum_MC_Logo361,362.jpg',
  'cardProdID': '362'
}]

const pwebCardList = [
  { 'benefit3': '',
    'cardName': '星展威士普卡',
    'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/dbs/DBS_Visa_Classic_logo100.jpg',
    'benefit1': '',
    'benefit2': '',
    'cardType': 'cash',
    'cardProdID': '100',
    'locale': 'zh',
    'moreInfo': 'https://dbsweb-u01-www.dbs.com.tw/personal-zh/cards/dbs-credit-cards/classic'
  },
  { 'benefit3': '持卡期間活利積分終身有效', 'cardName': '星展everyday白金卡', 'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/anz/Shop&Dine_Classic_Visa_Logo151.jpg', 'benefit1': '一般消費NT$25=1點活利積分', 'benefit2': '餐飲/指定量販超市/分期消費5倍回饋', 'cardType': 'bonus', 'cardProdID': '151', 'locale': 'zh', 'moreInfo': '' },
  { 'benefit3': '持卡期間飛行積金終身有效', 'cardName': '星展飛行鈦金卡', 'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/anz/Travel_Platinum_MC_Logo361,362.jpg', 'benefit1': '海外消費NT$20元=1點飛行積金；國內消費NT$30元=1點飛行積金', 'benefit2': '可兌換4大哩程獎勵計畫', 'cardType': 'fly', 'cardProdID': '361', 'locale': 'zh', 'moreInfo': 'https://dbsweb-u01-www.dbs.com.tw/personal-zh/cards/anz-credit-cards/travel-platinum' },
  { 'benefit3': '持卡期間活利積分終身有效', 'cardName': '星展everyday鈦金卡', 'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/anz/Life_Classic_MC_Logo340,341,342,343,344,345.jpg', 'benefit1': '一般消費NT$25=1點活利積分', 'benefit2': '餐飲/指定量販超市/分期消費5倍回饋', 'cardType': 'bonus', 'cardProdID': '342', 'locale': 'zh', 'moreInfo': '' },
  { 'benefit3': '持卡期間活利積分終身有效', 'cardName': '星展everyday白金卡', 'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/anz/Super_Platinum_Visa_Logo147.jpg', 'benefit1': '一般消費NT$25=1點活利積分', 'benefit2': '餐飲/指定量販超市/分期消費5倍回饋', 'cardType': 'bonus', 'cardProdID': '147', 'locale': 'zh', 'moreInfo': 'https://dbsweb-u01-www.dbs.com.tw/personal-zh/cards/anz-credit-cards/super-platinum' },
  { 'benefit3': '持卡期間飛行積金終身有效', 'cardName': '星展飛行鈦金卡', 'cardFace': 'https://dbsweb-u01-www.dbs.com.tw/iwov-resources/images/cards/anz/Travel_Platinum_MC_Logo361,362.jpg', 'benefit1': '海外消費NT$20元=1點飛行積金；國內消費NT$30元=1點飛行積金', 'benefit2': '可兌換4大哩程獎勵計畫', 'cardType': 'fly', 'cardProdID': '362', 'locale': 'zh', 'moreInfo': 'https://dbsweb-u01-www.dbs.com.tw/personal-zh/cards/anz-credit-cards/travel-platinum' }
]
const APPLY_STATUS_LIST = [
  '998',
  '997',
  '07',
  '09',
  '80',
  '56',
  '101',
  '18',
  '52', '53', '22', '59', '60',
  '02',
  '03'
]
const APPLY_LACK_STATUS_LIST = [
  '52', '53', '22', '59', '60'
]
const FILE_TYPE_LIST = [
  'A', 'B', 'C'
]

const ZH = [
  { id: 1, label: '基市', value: '10017000' },
  { id: 2, label: '北市', value: '63000000' },
  { id: 3, label: '新北市', value: '65000000' },
  { id: 4, label: '北縣', value: '10001000' },
  { id: 6, label: '桃市', value: '68000000' },
  { id: 8, label: '桃縣', value: '10003000' },
  { id: 9, label: '竹市', value: '10018000' },
  { id: 10, label: '竹縣', value: '10004000' },
  { id: 11, label: '苗縣', value: '10005000' },
  { id: 12, label: '中市', value: '66000000' },
  { id: 13, label: '中縣', value: '10006000' },
  { id: 14, label: '彰縣', value: '10007000' },
  { id: 15, label: '投縣', value: '10008000' },
  { id: 16, label: '雲縣', value: '10009000' },
  { id: 17, label: '嘉市', value: '10020000' },
  { id: 18, label: '嘉縣', value: '10010000' },
  { id: 19, label: '南市', value: '67000000' },
  { id: 20, label: '南縣', value: '10011000' },
  { id: 21, label: '高市', value: '64000000' },
  { id: 22, label: '高縣', value: '10012000' },
  { id: 23, label: '屏縣', value: '10013000' },
  { id: 24, label: '宜縣', value: '10002000' },
  { id: 25, label: '花縣', value: '10015000' },
  { id: 26, label: '東縣', value: '10014000' },
  { id: 27, label: '連江', value: '9007000' },
  { id: 28, label: '金門', value: '9020000' },
  { id: 29, label: '澎縣', value: '10016000' },
  { id: 30, label: '桃市', value: '68000000' }
]

// 未登入信用卡開卡
router.post('/01/ccd050101',
(req, res, next) => {
  const response = createResponse('CCD050101')
  response.header.sessionID = req.body.header.sessionID

  response.body.ccLogo = '150'
  response.body.ccBrand = '總該不會有問題了吧？'
  response.body.ccDesc = 'VISA Desc'
  response.body.allowOnBoard = 'Y'
  response.body.showESTMT = 'Y'
  response.body.extension = faker.phone.phoneNumber(),
  response.body.expireDay = '30'

  // response.header.returnCode = 'M0XXXX'
  // response.header.returnMessage = '其他錯誤呢？'
  // response.header.returnCode = 'M0101'
  // response.header.returnMessage = '您目前無法使用信用卡理財服務，請與本行客服聯繫(M0101)'
  // response.header.returnCode = 'M0029'
  // response.header.returnMessage = '您目前無法使用信用卡理財服務，請與本行客服聯繫(M0029)'
  res.json(response)
})

// 信用卡開卡
router.post('/01/ccd050102', (req, res, next) => {
  const response = createResponse('CCD050102')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 信用卡申請
router.post('/02/ccd050201', (req, res, next) => {
  const response = createResponse('CCD050201')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 未登入信用卡申請-身份驗證
router.post('/03/ccd050301', (req, res, next) => {
  const r = createResponse('CCD050301')
  r.header.sessionID = req.body.header.sessionID
  // apptype: 1-NTB(new user), 2-ETC(credit card user), 3-ETB(bank user)
  const body = {
    caseTemp: 'QQ20191022',
    apptype: '1',
    extension: faker.random.alphaNumeric(20),
    finacleMsg: null,
    specCode: null,
    specMsg: null
    // specCode: 'M0507',
    // specMsg: '您目前該信用卡申請單尚未完成，請先處理未完成的表單或是重新選擇信用卡'
  }
  r.body = body

  // r.header.returnCode = 'M0505'
  // r.header.returnMessage = 'This is bad error code...'

  res.json(r)
  // res.status(304).json({})
})

// 未登入信用卡申請-個人資本資料
router.post('/03/ccd050302', (req, res, next) => {
  const r = createResponse('CCD050302')
  r.header.sessionID = req.body.header.sessionID

  // r.header.returnCode = 'M0043'
  // r.header.returnMessage = '申請失敗，還不趕快投降，認輸．下一把會更好'

  res.json(r)
})

// 未登入信用卡申請-驗證它行信用卡
router.post('/03/ccd050303', (req, res, next) => {
  const r = createResponse('CCD050303')
  r.header.sessionID = req.body.header.sessionID

  // r.header.returnCode = 'M0043'
  // r.header.returnMessage = '申請失敗，還不趕快投降，認輸．下一把會更好'

  res.json(r)
})

// 未登入信用卡申請-職業資料
router.post('/03/ccd050304', (req, res, next) => {
  const r = createResponse('CCD050304')
  r.header.sessionID = req.body.header.sessionID

  // r.header.returnCode = 'M0099'
  // r.header.returnMessage = '系統發生錯誤，請聯絡客服人員'

  res.json(r)
})

// 未登入信用卡申請-特別商議條款與附卡
router.post('/03/ccd050305', (req, res, next) => {
  const r = createResponse('CCD050305')
  r.header.sessionID = req.body.header.sessionID
  r.body.isFileD = true //
  r.body.isFileE = true
  r.body.isFileF = true

  // r.header.returnCode = 'M4499'
  // r.header.returnMessage = '申請失敗，還不趕快投降，認輸．下一把會更好'

  res.json(r)
})

// 未登入信用卡申請-證件上傳
router.post('/03/ccd050306', (req, res, next) => {
  const r = createResponse('CCD050306')
  r.header.sessionID = req.body.header.sessionID

  r.body = {
    birthDay: '1992-03-18',
    idIssuePlace: randPickOneInArr(ZH).value,
    idIssueType: randPickOneInArr(['1', '2', '3']),
    // idIssueType: null,
    idIssueDate: '2000-01-04',
    fullhouseholdAddr: '',
    householdZIP: '114'
  }
  // r.body = {
  //   birthDay: null,
  //   idIssuePlace: null,
  //   // idIssueType: randPickOneInArr(['1', '2', '3']),
  //   idIssueType: null,
  //   idIssueDate: null,
  //   fullhouseholdAddr: '這個是11435的案例拉',
  //   householdZIP: '11435'
  // }

  // r.header.returnCode = 'M2200'
  // r.header.returnMessage = '上床失敗，請再好好努力'

  res.json(r)
})

// 未登入信用卡申請-送出申請
router.post('/03/ccd050307', (req, res, next) => {
  const r = createResponse('CCD050307')
  r.header.sessionID = req.body.header.sessionID

  // r.header.returnCode = 'M0505'
  // r.header.returnMessage = '工尛'

  res.json(r)
})

// 未登入信用卡申請-申請進度查詢身份驗證
router.post('/03/ccd050308', (req, res, next) => {
  const r = createResponse('CCD050308')
  r.header.sessionID = req.body.header.sessionID

  const body = {
    extension: 'X18Fo1RK9ebu7788'
  }
  r.body = body

  // r.header.returnCode = 'M1987'
  // r.header.returnMessage = '你是白痴-不能登入拉-M1987'
    res.json(r)
})

// 未登入信用卡申請-取得目前進度列表
router.post('/03/ccd050309', (req, res, next) => {
  const r = createResponse('CCD050309')
  r.header.sessionID = req.body.header.sessionID

  const body = {
    unSubmitList: pwebCardList.map((pCard, idx) => {
      return {
        cardList: allCardList,
        caseNo: `CP20191022${idx}`,
        lastSaveTime: moment().add(-5, 'days').format('YYYY-MM-DD HH:mm:ss'),
        caseStep: idx + 5,
        dueDate: moment().add((idx + 5) % 4, 'days').format('YYYY-MM-DD')
      }
    }),
    submitList: pwebCardList.map((pCard, idx) => {
      // const status = randPickOneInArr(APPLY_STATUS_LIST)
      const newArr = []
      var isAnyHasFollow = false
      allCardList.forEach(elem => {
        const status = randPickOneInArr(APPLY_STATUS_LIST)
        const hasFollow = APPLY_LACK_STATUS_LIST.includes(status)
        if (hasFollow) isAnyHasFollow = true
        var newObj = {
          ...elem,
          applyStatus: '',
          applyStatusCode: status
        }
        newArr.push(newObj)
      })
      let followList = []
      let count = 0
      if (isAnyHasFollow) {
        followList = execFnNTimes(_.random(1, 4), () => {
          const missDocCode = 'D' + _.random(1, 22)
          count++
          return {
            followCaseNo: `CP20191022${idx}`,
            FCseqNo: `CPFollowSeq${idx}_${count}`,
            fileType: randPickOneInArr(FILE_TYPE_LIST),
            fileTitle: faker.commerce.productName(),
            missDocCode: missDocCode,
            missDocDesc: missDocCode + ' Description...'
          }
        })
      }

      return {
        // cardLogo: pCard.cardProdID,
        // cardName: pCard.cardName,
        // cardURL: pCard.cardFace,
        cardList: newArr,
        caseNo: `CP20191022${idx}`,
        submitDate: moment().add(-10 + idx, 'days').format('YYYY-MM-DD HH:mm:ss'),
        followList
      }
    }),
    expireDay: '9'
  }
  r.body = body

  // r.header.returnCode = 'M1987'
  // r.header.returnMessage = '恩你就白痴 M1970 的音譯'

  res.json(r)
})

// 未登入信用卡申請-取消申請
router.post('/03/ccd050310', (req, res, next) => {
  const r = createResponse('CCD050310')
  r.header.sessionID = req.body.header.sessionID

  // r.header.returnCode = 'M1987'
  // r.header.returnMessage = '恩你就白痴 M1970 的音譯'

  res.json(r)
})

// 未登入信用卡申請-儲存申請資料
router.post('/03/ccd050311', (req, res, next) => {
  const r = createResponse('CCD050311')
  r.header.sessionID = req.body.header.sessionID

  const body = {
  }
  r.body = body

  // r.header.returnCode = 'M1003'
  // r.header.returnMessage = 'This is not your fault but ...'

  res.json(r)
})

// 未登入信用卡申請-ETB, ETC
router.post('/03/ccd050312', (req, res, next) => {
  const r = createResponse('CCD050312')
  r.header.sessionID = req.body.header.sessionID

  const body = {
    caseStep: 3,
    caseNo: 'CP20191022',
    nID: 'H122224164',
    cnName: '張曉萍',
    phoneNumber: '0917577538',
    email: 'sammi.change@gmail.com',
    cardLogo: '500',
    cardName: '',
    cardURL: '',
    enName: 'Sammi Chang',
    birthDay: '1995-03-18',
    eduLevel: '6',
    marriStatus: 'M',
    idIssuePlace: '10008000',
    idIssueType: '2',
    idIssueDate: '1995-09-05',
    residentStatus: '1',
    householdZIP: '23523',
    householdCity: '新北市',
    householdArea: '中和區',
    householdAddr: '阿斯福和路152號1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890',
    fullhouseholdAddr: '?',
    householdTEL: '029668877',
    // isHouseholdResidental: true,
    residentZIP: '23134',
    residentCity: '新北市',
    residentArea: '汐止區',
    residentAddr: '啊ㄉㄚ中山路200號',
    fullresidentAddr: '?',
    residentTEL: '028866996',
    businessCode: '1500',
    occCode: '28',
    companyNM: '-Thinkpower Info-',
    workYearMonth: '10010',
    companyDept: 'MAD',
    jobTitle: '大老闆',
    annualIncome: '10000000',
    preCompanyNM: '',
    preWYearMonth: '',
    companyZIP: '434',
    companyCity: '臺中市',
    companyArea: '龍井區',
    companyAddr: ' 瑞光路 --- 58號3-4樓',
    fullcompanyAddr: '?',
    companyTEL: '02-27714944',
    billDeliverType: '1',
    isESTMT: true,
    isCrossSell: true,
    isPinMailer: true,
    appSubcard: true,
    relationship: '2',
    subNid: 'Z299900104',
    subCNname: '張玉萍',
    subENname: 'Y.P Yuan',
    subBirthday: '1995-03-18',
    subMariStatus: 'M',
    subEduLevel: '5',
    subCompanyNM: ' 渣打',
    subMobile: '0911444555',
    subHomeTEL: '02-1998-1998',
    subcompanyTEL: '02-2772-0040',
    subIdIssuePlace: '10001000',
    subIdIssueType: '1',
    subIdIssueDate: '2001-03-18',
    nidFileA1: 'nidFileA1.png',
    nidFileA2: 'nidFileA2.png',
    incomeProfB1: 'incomeProfB1.png',
    // incomeProfB2: 'incomeProfB2.png',
    // incomeProfB3: 'incomeProfB3.png',
    // incomeProfB4: 'incomeProfB4.png',
    incomeProfB2: '',
    incomeProfB3: '',
    incomeProfB4: '',
    incomeProfB5: 'incomeProfB5.png',
    incomeProfB6: '',
    incomeProfB7: '',
    incomeProfB8: '',
    eSignC1: 'eSignC1.png',
    subNidFileD1: 'subNidFileD1.png',
    subNidFileD2: 'subNidFileD2.png',
    subeSignE1: 'subeSignE1.png'
  }
  r.body = body

  // r.header.returnCode = 'M1200'
  // r.header.returnMessage = '你以為蛇摸東西都會成功媽?'

  res.json(r)
})

// 未登入信用卡申請-上傳檔案/刪除檔案
router.post('/03/ccd050313', (req, res, next) => {
  const {
    body: {
      body: {
        uploadType,
        fileType,
        pic,
        picExtension
      }
    }
  } = req

  const r = createResponse('CCD050313')
  r.header.sessionID = req.body.header.sessionID

  if (pic && pic.length > 0) {
    r.body = { fileName: `f${uploadType}-${fileType}.${picExtension}` }
  } else {
    r.body = { fileName: '' }
  }

  // r.header.returnCode = 'M2200'
  // r.header.returnMessage = '上床失敗，請再好好努力'
  setTimeout(
    () => res.json(r),
    300
  )

})

// 未登入信用卡申請-補件上傳送出
router.post('/03/ccd050314', (req, res, next) => {
  const r = createResponse('CCD050314')
  r.header.sessionID = req.body.header.sessionID

  // r.header.returnCode = 'M4477'
  // r.header.returnMessage = '上傳發生錯誤拉'

  res.json(r)
})

// 未登入信用卡申請-下載已上傳檔案
router.post('/03/ccd050315', (req, res, next) => {
  const r = createResponse('CCD050315')
  r.header.sessionID = req.body.header.sessionID

  r.body = {
    uploadFile: 'R0lGODlhCgAKAPABAAAAAP///yH5BAHoAwEALAAAAAAKAAoAAAIUjI8HC9kKA5OmvYov3fNqLU3JGBQAOw==',
    fileExt: 'png'
  }

  res.json(r)
})

// 未登入信用卡申請-取得資料繼續填寫
router.post('/03/ccd050316', (req, res, next) => {
  const r = createResponse('CCD050316')
  r.header.sessionID = req.body.header.sessionID

  const body = {
    caseStep: 9,
    caseNo: 'CP20191022',
    apptype: '3',
    nID: 'H122224164',
    cnName: '張曉萍',
    phoneNumber: '0917577538',
    email: 'sammi.change@gmail.com',
    cardLogo: '200',
    cardName: '',
    cardURL: '',
    cardLogo2: '200',
    cardName2: '',
    cardURL2: '',
    cardLogo3: '200',
    cardName3: '',
    cardURL3: '',
    enName: 'Sammi Chang',
    birthDay: '1991-10-',
    eduLevel: '6',
    marriStatus: 'M',
    idIssuePlace: '10008000',
    idIssueType: '2',
    idIssueDate: '1995-09-05',
    residentStatus: '1',
    householdZIP: '235',
    householdCity: '新北市',
    householdArea: '中和區',
    householdAddr: '阿斯福和路152號',
    fullhouseholdAddr: '?',
    householdTEL: '02-9668877',
    isHouseholdResidental: true,
    residentZIP: '231',
    residentCity: '新北市',
    residentArea: '汐止區',
    residentAddr: '啊ㄉㄚ中山路200號',
    fullresidentAddr: '?',
    residentTEL: '02-8866996',
    businessCode: '1100',
    occCode: '15',
    companyNM: '-Thinkpower Info-',
    workYearMonth: '10011',
    companyDept: 'MAD',
    jobTitle: '大老闆',
    annualIncome: '10000000',
    preCompanyNM: '有的沒的',
    preWYearMonth: '99/01',
    companyZIP: '434',
    companyCity: '臺中市',
    companyArea: '龍井區',
    companyAddr: ' 瑞光路 --- 58號3-4樓',
    fullcompanyAddr: '?',
    companyTEL: '02-27714944',
    billDeliverType: '1',
    isESTMT: true,
    isCrossSell: true,
    isPinMailer: true,
    appSubcard: false,
    relationship: '2',
    subNid: null,
    subCNname: null,
    subENname: null,
    subBirthday: null,
    subMariStatus: null,
    subEduLevel: null,
    subCompanyNM: null,
    subMobile: null,
    subHomeTEL: null,
    subcompanyTEL: null,
    subIdIssuePlace: null,
    subIdIssueType: null,
    subIdIssueDate: null,
    // subNid: 'Z299900128',
    // subCNname: '張玉萍',
    // subENname: 'Y.P Yuan',
    // subBirthday: '1995-03-18',
    // subMariStatus: 'M',
    // subEduLevel: '5',
    // subCompanyNM: ' 渣打',
    // subMobile: '0911444555',
    // subHomeTEL: '02-19981998',
    // subcompanyTEL: '02-27720040',
    // subIdIssuePlace: '10001000',
    // subIdIssueType: '1',
    // subIdIssueDate: '2001-03-18',
    nidFileA1: 'osterA1.png',
    nidFileA2: 'osterA2.png',
    subNid: 'Z299900128',
    subCNname: '張玉萍',
    subENname: 'Y.P Yuan',
    subBirthday: '1995-03-18',
    subMariStatus: 'M',
    subEduLevel: '5',
    subCompanyNM: ' 渣打',
    subMobile: '0911444555',
    subHomeTEL: '02-19981998',
    subcompanyTEL: '02-27720040',
    subIdIssuePlace: '10001000',
    subIdIssueType: '1',
    subIdIssueDate: '2001-03-18',
    nidFileA1: 'osterA1.png',
    nidFileA2: 'osterA2.png',
    incomeProfB1: 'osterB1.png',
    // incomeProfB1: null,
    incomeProfB2: null,
    incomeProfB3: null,
    incomeProfB4: null,
    incomeProfB5: null,
    // eSignC1: 'osterC1.png',
    eSignC1: null,
    subNidFileD1: 'osterD1.png',
    subNidFileD2: 'osterD2.png',
    subeSignE1: 'osterE1.png',
    isNCCC: true,
    isFISC: false,
    cpCode: '',
    isJobChange: 'Y'

  }
  r.body = body

  // r.header.returnCode = 'M1987'
  // r.header.returnMessage = '恩你就白痴 M1970 的音譯'

  res.json(r)
})

// 未登入信用卡申請-附卡證件上傳
router.post('/03/ccd050317', (req, res, next) => {
  const r = createResponse('CCD050317')
  r.header.sessionID = req.body.header.sessionID

  r.body = {
    subNid: 'Z299900093',
    subCNname: '附卡人中文名',
    subBirthDay: '1994-10-12',
    // subIdIssuePlace: '',
    // subIdIssueType: null,
    // subIdIssueDate: null,
    subIdIssuePlace: randPickOneInArr(ZH).value,
    subIdIssueType: randPickOneInArr(['1', '2', '3']),
    subIdIssueDate: '2002-02-24',
    subFullhouseholdAddr: faker.address.streetAddress(true)
  }

  // r.header.returnCode = 'M2200'
  // r.header.returnMessage = '上床失敗，請再好好努力'

  res.json(r)
})

// 未登入信用卡申請-證件上傳送出
router.post('/03/ccd050318', (req, res, next) => {
  const r = createResponse('CCD050318')
  r.header.sessionID = req.body.header.sessionID
  // r.header.returnCode = 'M2200'
  // r.header.returnMessage = '上床失敗，請再好好努力'

  res.json(r)
})

router.post('/03/ccd050319', (req, res, next) => {
  const r = createResponse('CCD050319')
  r.header.sessionID = req.body.header.sessionID
  r.body = {
    extension: '0912345678'
  }
  res.json(r)
})

router.post('/03/ccd050320', (req, res, next) => {
  const r = createResponse('CCD050320')
  r.header.sessionID = req.body.header.sessionID
  res.json(r)
})

router.post('/03/ccd050321', (req, res, next) => {
  const r = createResponse('CCD050321')
  r.header.sessionID = req.body.header.sessionID
  res.json(r)
})

router.post('/03/ccd050322', (req, res, next) => {
  const r = createResponse('CCD050322')
  r.header.sessionID = req.body.header.sessionID
  r.body = {
    "cards": [
      {
        "cardProdID": "157",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "P",
        "cardKind": "N"
      },
      {
        "cardProdID": "153",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "C",
        "cardKind": "N"
      },
      {
        "cardProdID": "349",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "P",
        "cardKind": "N"
      },
      {
        "cardProdID": "365",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "C",
        "cardKind": "N"
      },
      {
        "cardProdID": "205",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "P",
        "cardKind": "N"
      },
      {
        "cardProdID": "201",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "C",
        "cardKind": "N"
      },
      {
        "cardProdID": "405",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "P",
        "cardKind": "N"
      },
      {
        "cardProdID": "402",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "C",
        "cardKind": "N"
      },
      {
        "cardProdID": "356",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "P",
        "cardKind": "N"
      },
      {
        "cardProdID": "360",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "C",
        "cardKind": "N"
      },
      {
        "cardProdID": "184",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "P",
        "cardKind": "I"
      },
      {
        "cardProdID": "181",
        "cardName": null,
        "cardFace": null,
        "benefit1": null,
        "benefit2": null,
        "benefit3": null,
        "locale": null,
        "moreInfo": null,
        "cardType": null,
        "cardSubType": "C",
        "cardKind": "I"
      }
    ],
    "groups": [
      {
        "id": "20200502001",
        "cards": [
          "157",
          "153"
        ]
      },
      {
        "id": "20200502002",
        "cards": [
          "349",
          "365"
        ]
      },
      {
        "id": "20200502005",
        "cards": [
          "405",
          "402"
        ]
      },
      {
        "id": "20200502006",
        "cards": [
          "356",
          "360"
        ]
      },
      {
        "id": "20200502003",
        "cards": [
          "205",
          "201"
        ]
      },
      {
        "id": "20200502004",
        "cards": [
          "184",
          "181"
        ]
      }
    ]
  }
  res.json(r)
})

module.exports = router