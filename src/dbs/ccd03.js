const express = require('express')
const router = express.Router()
const faker = require('faker')
const Duplicate = require('../dbs-lib/FnDuplicate.js')
const createResponse = require('../dbs-lib/common-response').createResponse
const RandomArray = require('../dbs-lib/FnRandomArray.js')

// 未登入線上繳費:全國繳費網-用戶資料驗證
router.post('/01/ccd030101', (req, res, next) => {
  const response = createResponse('CCD030101')

  res.json(response)
})

// 未登入線上繳費:查詢本期應繳金額
router.post('/01/ccd030102', (req, res, next) => {
  const response = createResponse('CCD030102')
  response.header.sessionID = req.body.header.sessionID

  const amt = faker.finance.amount(1000, 100000, 0)
  const data = {
    amtNewPurchases: amt, // 本期新增金額 = (前期應繳-前期已繳+本期應繳)
    amtCurrDue: amt, // 本期全部應繳金額
    amtCurrPayment: 0, // 本期累積已繳金額
    amtMinPayment: amt / 2 // 本期最低應繳金額
  }
  response.body = data

  // response.body = {}
  // response.header.returnCode = 'M9801-S001-9'
  // response.header.returnMessage = 'Core API回覆查無帳單資料'
  res.json(response)
})

// 查詢銀行代碼清單
router.post('/01/ccd030103', (req, res, next) => {
  const response = createResponse('CCD030103')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    bankList: Duplicate.map(10, () => ({
      bankNO: faker.finance.amount(100, 999, 0), // 銀行代碼(3碼)
      bankName: faker.finance.accountName() // 銀行名稱
    }))
  }

  data.bankList.push({ bankNO: '099', bankName: 'sample bank' })
  response.body = data

  res.json(response)
})

// 未登入線上繳費:全國繳費網-繳款
router.post('/01/ccd030104', (req, res, next) => {
  const response = createResponse('CCD030104')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    txSEQ: faker.random.uuid(), // 交易序號 Channel_Tx_Ref (CCDS generated UUID)
    paymentNO: 'A' + '1' + faker.random.number({ 'min': 10000000, 'max': 99999999 }), // 繳款編號(National ID證號或信用卡號)
    amt: faker.finance.amount(1000, 100000, 0), // 繳款金額
    bankNO: '813', // 轉出銀行代碼(3碼)
    acctNO: faker.finance.account
  }
  response.body = data

  res.json(response)
})

// 取得便利商店繳款條碼
router.post('/02/ccd030201', (req, res, next) => {
  const response = createResponse('CCD030201')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    barCodeNO1: '501231C77', // 條碼一:商店代碼
    barCodeNO2: '7756005788823346', // 條碼二:代收編號
    barCodeNO3: '01221X000016982', // 條碼三:金額代碼(應繳總額)
    barCodeNO4: '01221X000016982' // 條碼四:金額代碼(最底應繳金額)
  }
  response.body = data

  res.json(response)
})

// 查詢電子帳單
router.post('/03/ccd030301', (req, res, next) => {
  const response = createResponse('CCD030301')
  const nowDate = new Date()
  response.header.sessionID = req.body.header.sessionID
  let times = 0
  const data = {
    eBillList: Duplicate.map(2, () => {
      times++
      return ({
        eBillNO: faker.random.number({ 'min': 10000000, 'max': 99999999 }), // 帳單流水序號
        eBillYYYY: '2017', // 帳單日期-年 YYYY
        eBillMM: (nowDate.getMonth() + 1 - times).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }), // 帳單日期-月 MM
        eBillFromDate: '20170401', // 帳單起日YYYYMMDD
        eBillToDate: '20170430', // 帳單迄日YYYYMMDD
        eBillFileName: `${(nowDate.getMonth() + 1 - times).toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false })}月電子帳單`
      })
    }),
    statmFlag: 'N'
  }
  response.body = {} // 模擬statmFlag != 'Y'
  // response.body = data
  res.json(response)
})

// 線上繳費:其他銀行帳戶繳款-用戶資料驗證
router.post('/04/ccd030401', (req, res, next) => {
  const response = createResponse('CCD030401')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 線上繳費:其他銀行帳戶繳款-繳款
router.post('/04/ccd030402', (req, res, next) => {
  const response = createResponse('CCD030402')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    txSEQ: faker.random.uuid(),
    nID: 'A' + '1' + faker.random.number({ 'min': 10000000, 'max': 99999999 }), // 繳款編號(National ID證號或信用卡號),
    amt: req.body.body.amt,
    bankNO: req.body.body.bankNO,
    acctNO: '00000000' + req.body.body.acctNO
  }
  response.body = data
  res.json(response)
})

// 取得活期帳戶
router.post('/05/ccd030501', (req, res, next) => {
  const response = createResponse('CCD030501')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    acctList: Duplicate.map(faker.random.number({ 'min': 3, 'max': 5 }), () => ({
      // acctList: Duplicate.map(faker.random.number({'min': 1, 'max': 1}), () => ({
      acctNO: faker.finance.account(),
      acctBalance: faker.finance.amount(10000, 1000000, 0),
      acctName: faker.finance.accountName()
    }))
  }
  response.body = data
  res.json(response)
})

// 活期帳戶繳款-用戶驗證
router.post('/05/ccd030502', (req, res, next) => {
  const response = createResponse('CCD030502')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 活期帳戶繳款-繳款
router.post('/05/ccd030503', (req, res, next) => {
  const response = createResponse('CCD030503')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    txSEQ: faker.random.uuid(),
    amt: req.body.body.amt,
    acctNO: req.body.body.acctNO
  }
  response.body = data
  res.json(response)
})

// 信用卡自動扣款查詢
router.post('/06/ccd030601', (req, res, next) => {
  const response = createResponse('CCD030601')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    autoPayFlag: RandomArray.get(['Y', 'N']) // 是否設定自動扣款 Y:是 N:無
  }
  response.body = data
  res.json(response)
})

// 查詢銀行代碼清單 新
router.post('/01/ccd030105', (req, res, next) => {
  const response = createResponse('CCD030105')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    bankList: Duplicate.map(10, () => ({
      bankNO: faker.finance.amount(100, 999, 0), // 銀行代碼(3碼)
      bankName: faker.finance.accountName() // 銀行名稱
    }))
  }

  data.bankList.push({ bankNO: '099', bankName: 'sample bank' })
  response.body = data

  res.json(response)
})

module.exports = router
