const express = require('express')
const faker = require('faker')
const merge = require('lodash/merge')
const path = require('path')
const fs = require('fs')
const moment = require('moment')
const sortBy = require('lodash/sortBy')

const Duplicate = require('../dbs-lib/FnDuplicate.js')
const RandomArray = require('../dbs-lib/FnRandomArray.js')
const RandomArrayFunc = require('../dbs-lib/FnRandomArrayFunc.js')
const createResponse = require('../dbs-lib/common-response').createResponse

const router = express.Router()
const yesNoArray = ['Y', 'N']

// 帳單總覽
const unBilledForeignExpenses = [() => ({}), () => ({
  orglAmt: faker.finance.amount(100, 10000, 0), // 原始消費金額
  orglCry: 'USD' // 原始消費幣別
}), () => ({
  orglAmt: faker.finance.amount(250, 25000, 0), // 原始消費金額
  orglCry: 'JPY' // 原始消費幣別
})]

// 取得使用者信用卡資訊
const cards = [
  {ccNO: '5915310333250090', cardName: '飛行卡', ccFlag: 'S', ccLogo: '110', groupSeq: 0},
  {ccNO: '8959742627339438', cardName: '豐盛御璽卡', ccFlag: 'S', ccLogo: '150', groupSeq: 1},
  {ccNO: '1234567890123456', cardName: '測試卡', ccFlag: '', ccLogo: '160', groupSeq: 2},
  {ccNO: '2434624722460285', cardName: '豐盛御璽卡', ccFlag: 'M', ccLogo: '150', groupSeq: 1},
  {ccNO: '6874634743668139', cardName: '飛行卡', ccFlag: 'M', ccLogo: '110', groupSeq: 0},
]

const isVirtualCardArray = ['Y', 'N']

// 卡片block code
const ccBlockStatusArray = ['', 'B', 'C', 'E', 'F', 'G', 'I', 'J', 'K', 'N', 'O', 'S', 'T', 'X']

router.post('/01/ccd020101', (req, res, next) => {
  const ccd020101ResponseFunc = () => {
    const response = createResponse('CCD020101')
    response.header.sessionID = req.body.header.sessionID
    const data = {
      stmtYM: moment().add(-1, 'months').format('YYYYMM'), // 帳單月份YYYYMM
      stmtCycleDate: `${moment().add(-1, 'months').format('YYYYMM')}25`, // 帳單結帳日 YYYYMMDD
      paymentDueDate: `${moment().format('YYYYMM')}01`, // 繳款截止日 YYYYMMDD
      creditLine: '', // 信用額度
      paymentPeriodStart: `${moment().add(-1, 'months').format('YYYYMM')}06`, // 入帳期間起YYYYMMDD
      paymentPeriodEnd: `${moment().add(-1, 'months').format('YYYYMM')}25`, // 入帳期間迄YYYYMMDD
      amtPastDue: faker.finance.amount(1000, 19999, 0).toString(), // 前期應繳金額
      amtPayment: faker.finance.amount(1000, 19999, 0).toString(), // 前期繳款金額
      amtNewPurchases: faker.finance.amount(1000, 19999, 0).toString(), // 本期新增金額
      amtCurrDue: '0', // faker.finance.amount(1000, 19999, 0).toString(), // 本期全部應繳金額
      amtCurrPayment: '4000', // faker.finance.amount(1000, 19999, 0).toString(), // 本期已繳金額
      amtMinPayment: '3000', // faker.finance.amount(1000, 19999, 0).toString(), // 本期最低應繳金額
      domCashAvail: (20 * 10000).toString(), // 預借現金額度
      overseasCashAvail: (10 * 10000).toString(), // 國外預借現金額度
      creditRate: '11.33', // 信用年利率 11.33
      interestExpiryDate: `${moment().add(-1, 'months').format('YYYYMM')}`, // 利率適用期限 YYYYMMDD
      rewardsList: Duplicate.map(
        faker.random.number({ 'min': 1, 'max': 6 }),
        () => ({
          rewardScheme: '類別',
          rewardName: '活利績點',
          previousPoints: faker.finance.amount(0, 20000, 0).toString(),
          newPointsEarned: faker.finance.amount(0, 20000, 0).toString(),
          adjustedPoints: faker.finance.amount(0, 20000, 0).toString(),
          redeemedPoints: faker.finance.amount(0, 20000, 0).toString(),
          expiredPoints: faker.finance.amount(0, 20000, 0).toString(),
          currentPoints: faker.finance.amount(0, 20000, 0).toString(),
          rewardDesc: moment().add(6, 'M').format('YYYY/MM/DD')
        })
      ),
      cycleDueCode: '1', // RandomArray.get(['0', '1']),
      orglCycleDueCode: '1', // RandomArray.get(['', 0', '1', '2']),
      paymentTXList: [
        {
          txDate: `${moment().format('YYYYMM')}03`,
          postDate: `${moment().format('YYYYMM')}04`,
          txDesc: 'ATM轉帳繳款',
          txAmt: -1 * faker.finance.amount(1000, 100000, 0).toString()
        },
        {
          txDate: `${moment().format('YYYYMM')}03`,
          postDate: `${moment().format('YYYYMM')}03`,
          txDesc: '現金回饋',
          txAmt: -1 * faker.finance.amount(10, 100, 0).toString()
        }
      ]
    }
    response.body = data
    // response.header.returnCode = 'M9801-S001'
    // response.header.returnMessage = '取得紅利資料失敗'

    res.json(response)
  }

  setTimeout(ccd020101ResponseFunc, 6 * 1000)
})

// 未出帳帳單交易明細
router.post('/02/ccd020202', (req, res, next) => {
  const response = createResponse('CCD020202')
  const ccID = req.body.body.ccID
  response.header.sessionID = req.body.header.sessionID
  const data = {
    unbillTXDetail: []
  }
  const unbillTXDetail = []
  if (ccID) {
    cards.map((card, index) => {
      if ((index + 1).toString() === ccID) {
        const item = {}
        item.ccNO = card.ccNO
        item.ccFlag = card.ccFlag
        item.cardName = card.cardName
        item.ccLogo = card.ccLogo
        item.ccBrand = 'VISA'
        item.ccDesc = 'VISA Desc'
        item.unbillTXList = Duplicate.map(RandomArray.get([10, 20]), () => merge({
          txDate: '20170501', // 消費日YYYYMMDD
          postDate: '20170504', // 入帳日YYYYMMDD
          txDesc: faker.lorem.sentence(), // 消費明細
          txAmt: faker.finance.amount(1000, 100000, 0) // 消費金額(NTD)
        }, RandomArrayFunc.get(unBilledForeignExpenses)))
        item.ccID = (index + 1).toString()

        unbillTXDetail.push(item)
      }
    })
  } else {
    // 從cards取出信用卡資訊
    cards.map((card, index) => {
      const item = {}
      item.ccNO = card.ccNO
      item.ccFlag = card.ccFlag
      item.cardName = card.cardName
      item.ccLogo = card.ccLogo
      item.ccBrand = 'VISA'
      item.ccDesc = 'VISA Desc'
      item.unbillTXList = Duplicate.map(RandomArray.get([10, 20]), () => merge({
        txDate: '20170501', // 消費日YYYYMMDD
        postDate: '20170504', // 入帳日YYYYMMDD
        txDesc: faker.lorem.sentence(), // 消費明細
        txAmt: faker.finance.amount(1000, 100000, 0) // 消費金額(NTD)
      }, RandomArrayFunc.get(unBilledForeignExpenses)))
      item.ccID = (index + 1).toString()

      unbillTXDetail.push(item)
    })
  }
  data.unbillTXDetail = unbillTXDetail

  response.body = data
  res.json(response)
})

// 本期帳單交易明細
const foreignExpenses = [() => ({}), () => ({
  txFAmt: faker.finance.amount(100, 10000, 0), // 原始消費金額
  txCurrency: 'USD' // 原始消費幣別
})]

router.post('/03/ccd020301', (req, res, next) => {
  const response = createResponse('CCD020301')
  response.header.sessionID = req.body.header.sessionID
  const dateNow = new Date()
  const data = {
    stmtYM: `${dateNow.getFullYear()}${dateNow.getMonth().toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping: false})}`,
    cardList: cards.map(card => {
      const currentTXList = Duplicate.map(faker.random.number({'min': 1, 'max': 10}), () => {
        const startDate = new Date(dateNow.getFullYear(), dateNow.getMonth() - 1, 1)
        const endDate = new Date(dateNow.getFullYear(), dateNow.getMonth() - 1, 20)
        const txDate = faker.date.between(startDate, endDate)
        return merge({
          txDate: moment(txDate).format('YYYYMMDD'),
          postDate: moment(txDate).add(3, 'days').format('YYYYMMDD'),
          txDesc: faker.lorem.sentence(),
          txAmt: faker.finance.amount(1000, 100000, 0),
          orglAmt: faker.finance.amount(1000, 100000, 2) * faker.random.number({'min': -1, 'max': 1}),
          orglCry: 'US'
        }, RandomArrayFunc.get(foreignExpenses))
      })
      return {
        ccFlag: card.ccFlag,
        ccNO: card.ccNO,
        cardName: card.cardName,
        ccLogo: RandomArray.get(['110', '150', '10']),
        ccBrand: 'VISA',
        ccDesc: 'VISA Desc',
        currentTXList: sortBy(currentTXList, ['txnDate'])
      }
    })
  }
  response.body = data

  // response.header.returnCode = 'M9801-S002'
  // response.header.returnMessage = '無法取得卡片資訊'

  res.json(response)
})

// 下載電子帳單
router.post('/04/ccd020401', (req, res, next) => {
  fs.readFile(path.resolve(__dirname, '../assets/oster.pdf'), function (err, data) {
    const response = createResponse('CCD020401')
    response.header.sessionID = req.body.header.sessionID
    const resData = {
      eBillFile: (err) ? '' : Buffer.from(data).toString('base64')
    }
    response.body = resData
    res.json(response)
  })
})

// 申請電子帳單
router.post('/04/ccd020402', (req, res, next) => {
  const response = createResponse('CCD020402')
  response.header.sessionID = req.body.header.sessionID
  response.body = {}

  // response.header.returnCode = 'M0987'
  // response.header.returnMessage = '我真的好白痴'

  res.json(response)
})

// 取得使用者資訊
router.post('/06/ccd020601', (req, res, next) => {
  const delayResponse = () => {
    const response = createResponse('CCD020601')
    response.header.sessionID = req.body.header.sessionID

    const data = {
      emailDetl: {
        emailID: 'PER',
        email: null // faker.lorem.word() + faker.internet.email()
      },
      phoneDetl: {
        phoneID: '01',
        phoneCtryCode: null, // '65',
        phoneNumber: '+886987***321', // faker.phone.phoneNumber().replace(/-/g, ''),
        updatedPhoneCtryCode: '+886',
        updatedPhoneNumber: '0987***321'
      },
      addressDetl: {
        addressID: 'HOM',
        zipCode: faker.address.zipCode(),
        address: '台北市內湖區瑞光路583巷25號1樓',
        updatedZipCode: faker.address.zipCode(),
        updatedAddress: '台北市內湖區瑞光路399號'
        // updatedZipCode: null,
        // updatedAddress: null
      },
      blockCode: 'blockCode',
      statmFlag: 'N',
      userCode: faker.lorem.word().toUpperCase(), // 'A123456789' 與帳號同
      // userCode: 'A123456789', // faker.lorem.word(), // 'A123456789' 與帳號同
      nID: 'A123456789', // 'A' + '1' + faker.random.number({'min': 10000000, 'max': 99999999}),
      nickname: null,
      fpcFlag: 'N', // RandomArray.get(yesNoArray),
      rmsFlag: 'N', // RandomArray.get(yesNoArray),
      userType: 'C' // RandomArray.get(['A', 'B', 'C'])
    }
    response.body = data

    // response.header.returnCode = 'M0888-978'
    // response.header.returnMessage = '由V+取得使用者資訊發生錯誤'

    res.json(response)
  }

  setTimeout(delayResponse, 2000)
})

router.post('/06/ccd020602', (req, res, next) => {
  const response = createResponse('CCD020602')
  response.header.sessionID = req.body.header.sessionID
  const ccList = cards.map((card, index) => ({
    cardName: card.cardName, // 卡片名稱
    ccNO: card.ccNO, // 正卡卡號
    ccBrand: 'VISA', // 信用卡品牌
    ccLogo: card.ccLogo, // 信用卡的產品代碼(系統用來辨別哪一張卡片，與PWeb串接會用此代碼)
    ccDesc: 'VISA Desc', // faker.lorem.sentence(), // 卡片介紹
    isVirtualCard: RandomArray.get(isVirtualCardArray), // 是否為虛擬卡
    expDate: '0221', // 信用卡效期mmyy
    ccFlag: card.ccFlag, // M：主卡, S：附卡
    ccStatus: '3', // faker.random.number({'min': 0, 'max': 2}).toString(), // 卡片狀態 (0&2需再參照ccBlockStatus)
    ccID: (index + 1).toString(),
    ccBlockStatus: 'A', // RandomArray.get(ccBlockStatusArray),  // 卡片block code 'A,C,D,E,F,G,H,J,N,O,R,T,V'
    groupSeq: card.groupSeq
  }))

  const data = {
    ccList
  }
  response.body = data

  res.json(response)
})

// Home Landing
router.post('/05/ccd020501', (req, res, next) => {
  const response = createResponse('CCD020501')
  response.header.sessionID = req.body.header.sessionID
  const ccList = cards.map((card, index) => ({
    cardName: card.cardName, // 卡片名稱
    ccNO: card.ccNO, // 正卡卡號
    ccBrand: 'VISA', // 信用卡品牌
    ccLogo: card.ccLogo, // 信用卡的產品代碼(系統用來辨別哪一張卡片，與PWeb串接會用此代碼)
    ccDesc: 'VISA Desc', // faker.lorem.sentence(), // 卡片介紹
    isVirtualCard: RandomArray.get(isVirtualCardArray), // 是否為虛擬卡
    expDate: '0221', // 信用卡效期mmyy
    oldexpDate: '', // 舊有效效期
    ccFlag: 'M', // card.ccFlag, // M：主卡, S：附卡
    ccStatus: faker.random.number({'min': 0, 'max': 2}).toString(), // 卡片狀態 (0&2需再參照ccBlockStatus)
    ccID: (index + 1).toString(),
    ccBlockStatus: '',  // 卡片block code
    groupSeq: card.groupSeq
  }))

  
  const data = {
    emailDetl: {
      emailID: 'PER',
      email: null // faker.lorem.word() + faker.internet.email()
    },
    phoneDetl: {
      phoneID: '01',
      phoneCtryCode: null, // '65',
      phoneNumber: '+886987***321', // faker.phone.phoneNumber().replace(/-/g, ''),
      updatedPhoneCtryCode: '+886',
      updatedPhoneNumber: '0987***321'
    },
    addressDetl: {
      addressID: 'HOM',
      zipCode: faker.address.zipCode(),
      address: '台北市內湖區瑞光路583巷25號1樓',
      updatedZipCode: faker.address.zipCode(),
      updatedAddress: '台北市內湖區瑞光路399號'
    },
    blockCode: 'blockCode',
    statmFlag: 'N',
    userCode: faker.lorem.word().toUpperCase(), // 'A123456789' 與帳號同
    // userCode: 'A123456789', // faker.lorem.word(), // 'A123456789' 與帳號同
    nID: 'A123456789', // 'A' + '1' + faker.random.number({'min': 10000000, 'max': 99999999}),
    nickname: null,
    fpcFlag: 'N', // RandomArray.get(yesNoArray),
    rmsFlag: 'N', // RandomArray.get(yesNoArray),
    userType: 'C', // RandomArray.get(['A', 'B', 'C'])
    ccList,
    amtCurrPayment: '12345',
    unbillStart: '27', // 未出帳起日DD
    ccLimit: 20 * 10000, // 信用額度
    unbillAmt: faker.finance.amount(1000, 100000, 0), // 未出帳金額
    avlBalance: faker.finance.amount(1000, 100000, 0), // available balance可用餘額
    rPoints: faker.finance.amount(0, 10000, 0).toString(), // 紅利點數
    mPoints: faker.finance.amount(0, 10000, 0).toString(), // 飛行積金
    crPoints: faker.finance.amount(0, 10000, 0).toString() // 現金積點
  }
  response.body = data

  res.json(response)
})

module.exports = router
