const express = require('express')
const router = express.Router()
const faker = require('faker')
const moment = require('moment')
const createResponse = require('../dbs-lib/common-response').createResponse
const RandomArray = require('../dbs-lib/FnRandomArray.js')

const dateNow = new Date()
const startDate = new Date(dateNow.getFullYear(), dateNow.getMonth() - 1, 1)
const endDate = new Date(dateNow.getFullYear(), dateNow.getMonth() + 1, 20)

const urls = [
  null,
  'http://www.google.com.tw',
  'http://bitbucket.org',
  'http://www.facebook.com',
  'https://twitter.com/?lang=zh-tw',
  'https://www.instagram.com'
]
const activityNames = [
  '好禮大回餽，年度消費滿１０萬，給你大驚喜',
  '豐神禮卷獎不完',
  '冬天禮品，進補您的身體',
  '[影城禮卷] 熱門電影馬上看，各大影城通用禮卷，快來領取吧！',
  '我們需要更多晶礦',
  'VIP大回餽-自己選擇的路，再荒謬也要走完',
  '翔神安左教學體驗課程',
  '運動咖專屬好康，大相報',
  '這個活動，給長期支援我們的卡友，一個最長長99的禮物喔！'
]

// 查詢未登錄行銷活動
router.post('/01/ccd080101', (req, res, next) => {
  const response = createResponse('CCD080101')
  response.header.sessionID = req.body.header.sessionID
  const data = {notRegActivity: []}

  // const dataAmount = 0 // faker.random.number({ 'min': 3, 'max': 10 })
  const dataAmount = faker.random.number({ 'min': 3, 'max': 10 })
  for (var index = 0; index < dataAmount; index++) {
    data.notRegActivity.push({
      activityCode: faker.random.uuid(),
      activityName: RandomArray.get(activityNames),
      startTime: moment(faker.date.between(startDate, endDate)).add(-2, 'days').format('YYYYMMDDHHmmss'),
      endTime: moment(faker.date.between(startDate, endDate)).add(3, 'days').format('YYYYMMDDHHmmss'),
      regStartTime: moment(faker.date.between(startDate, endDate)).add(-2, 'days').format('YYYYMMDDHHmmss'),
      regEndTime: moment(faker.date.between(startDate, endDate)).add(3, 'days').format('YYYYMMDDHHmmss'),
      activityUrl: RandomArray.get(urls)
    })
  }

  response.body = data
  setTimeout(() => res.json(response), 3000)
})

// 活動登錄
router.post('/02/ccd080201', (req, res, next) => {
  const response = createResponse('CCD080201')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  response.header.returnMessage = '成功變更'
  // response.header.returnCode = 'M7788'
  // response.header.returnMessage = '失敗啦……'
  res.json(response)
})

// 查詢已登錄行銷活動
router.post('/03/ccd080301', (req, res, next) => {
  const response = createResponse('CCD080301')
  response.header.sessionID = req.body.header.sessionID
  const data = {regActivityHistory: []}

  const dataAmount = faker.random.number({ 'min': 10, 'max': 20 })
  for (var index = 0; index < dataAmount; index++) {
    data.regActivityHistory.push({
      activityCode: faker.random.uuid(),
      activityName: RandomArray.get(activityNames),
      receiveTime: moment(faker.date.between(startDate, endDate)).add(-2, 'days').format('YYYYMMDD'),
      activityUrl: RandomArray.get(urls)
    })
  }

  response.body = data
  res.json(response)
})

module.exports = router
