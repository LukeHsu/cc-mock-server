const express = require('express')
const router = express.Router()

router.get('/', (req, res, next) => {
  res.render('ib', {id: req.query.id, userid: req.query.userid})
  res.end()
})

router.post('/', (req, res, next) => {
  res.render('ib', {id: req.body.id, userid: req.body.userid})
  res.end()
})

module.exports = router
