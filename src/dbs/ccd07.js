const express = require('express')
const router = express.Router()
const faker = require('faker')
const moment = require('moment')
const Duplicate = require('../dbs-lib/FnDuplicate.js')
const createResponse = require('../dbs-lib/common-response').createResponse
const RandomArray = require('../dbs-lib/FnRandomArray.js')
const execFnNTimes = require('../dbs-lib/execFnNTimes')
const dateNow = new Date()
const startDate = new Date(dateNow.getFullYear(), dateNow.getMonth() - 1, 1)
const endDate = new Date(dateNow.getFullYear(), dateNow.getMonth() - 1, 20)
const _ = require('lodash')

const cards = [
  { ccNO: '5915310333250090', cardName: '飛行卡', ccFlag: 'S', ccLogo: '110', groupSeq: 0, ccBrand: 'VISA' },
  { ccNO: '8959742627339438', cardName: '豐盛御璽卡', ccFlag: 'S', ccLogo: '150', groupSeq: 1, ccBrand: 'MasterCard' },
  { ccNO: '1234567890123456', cardName: '測試卡', ccFlag: '', ccLogo: '160', groupSeq: 2, ccBrand: 'JCB' },
  { ccNO: '2434624722460285', cardName: '豐盛帥哥卡', ccFlag: 'M', ccLogo: '150', groupSeq: 1, ccBrand: 'VISA' },
  { ccNO: '6874634743668139', cardName: '飛行卡', ccFlag: 'M', ccLogo: '110', groupSeq: 0, ccBrand: 'MasterCard' },
]

// 查詢可分期合格交易
router.post('/01/ccd070101', (req, res, next) => {
  const response = createResponse('CCD070101')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    eligibleTransactionsList: []
  }

  const dataAmount = 2//faker.random.number({ 'min': 1, 'max': 5 })
  for (var index = 0; index < dataAmount; index++) {
    const card = cards[index]

    data.eligibleTransactionsList.push({
      accountNO: faker.finance.account(16),
      cardName: card.name,
      ccLogo: card.ccLogo,
      ccBrand: card.ccBrand,
      accountSum: faker.finance.amount(1000, 10000, 0),
      accountTransactionList: Duplicate.map(RandomArray.get([5, 10]), () => {
        return {
          transactionReference: faker.random.uuid(),
          transactionDate: moment(faker.date.between(startDate, endDate)).add(-2, 'days').format('YYYYMMDD'),
          postingDate: moment(faker.date.between(startDate, endDate)).add(3, 'days').format('YYYYMMDD'),
          transactionType: 'type',
          transactionAmount: faker.finance.amount(100, 10000, 0),
          transactionCurrency: 'NT',
          transactionDescription: faker.lorem.words(faker.random.number({ 'min': 3, 'max': 6 })),
          transactionCodeDescription: faker.lorem.words(faker.random.number({ 'min': 3, 'max': 6 })),
          ccNO: card.ccNO,
          ccID: `CCID-${card.ccNO}`,
          planList: ['3', '6', '9'].map((months) => {
            return ({
              planId: 'plan-' + months,  // 為方便測試將planId與months設相同
              months: months,
              sbiInterestRate: faker.random.number({ 'min': 3, 'max': 6 })
            })
          })
        }
      })
    })
  }

  response.body = []
  // response.body = data
  // response.header.returnCode = 'M9800'
  // response.header.returnMessage = 'M98　測試看看嘍！'
  setTimeout(() => res.json(response), 3000)
})

// 查詢可分期合格交易
router.post('/01/ccd070102', (req, res, next) => {
  const response = createResponse('CCD070102')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    eligibleTransactionsList: []
  }

  const dataAmount = 2//faker.random.number({ 'min': 1, 'max': 5 })
  for (var index = 0; index < dataAmount; index++) {
    const card = cards[index]

    data.eligibleTransactionsList.push({
      accountNO: faker.finance.account(16),
      cardName: card.name,
      ccLogo: card.ccLogo,
      ccBrand: card.ccBrand,
      accountSum: faker.finance.amount(1000, 10000, 0),
      accountTransactionList: Duplicate.map(RandomArray.get([5, 10]), () => {
        if (index == 1) {
          return {
            transactionReference: faker.random.uuid(),
            transactionDate: moment(faker.date.between(startDate, endDate)).add(-2, 'days').format('YYYYMMDD'),
            postingDate: moment(faker.date.between(startDate, endDate)).add(3, 'days').format('YYYYMMDD'),
            transactionType: 'type',
            transactionAmount: faker.finance.amount(100, 10000, 0),
            transactionCurrency: 'NT',
            transactionDescription: faker.lorem.words(faker.random.number({ 'min': 3, 'max': 6 })),
            transactionCodeDescription: faker.lorem.words(faker.random.number({ 'min': 3, 'max': 6 })),
            ccNO: card.ccNO,
            ccID: `CCID-${card.ccNO}`,
            planList: ['3', '6', '12', '24'].map((months) => {
              return ({
                planId: 'plan-' + months,  // 為方便測試將planId與months設相同
                months: months,
                sbiInterestRate: faker.random.number({ 'min': 3, 'max': 6 })
              })
            })
          }
        }



        return {
          transactionReference: faker.random.uuid(),
          transactionDate: moment(faker.date.between(startDate, endDate)).add(-2, 'days').format('YYYYMMDD'),
          postingDate: moment(faker.date.between(startDate, endDate)).add(3, 'days').format('YYYYMMDD'),
          transactionType: 'type',
          transactionAmount: faker.finance.amount(100, 10000, 0),
          transactionCurrency: 'NT',
          transactionDescription: faker.lorem.words(faker.random.number({ 'min': 3, 'max': 6 })),
          transactionCodeDescription: faker.lorem.words(faker.random.number({ 'min': 3, 'max': 6 })),
          ccNO: card.ccNO,
          ccID: `CCID-${card.ccNO}`,
          planList: ['3', '6', '9', '12', '18', '24'].map((months) => {
            return ({
              planId: 'plan-' + months,  // 為方便測試將planId與months設相同
              months: months,
              sbiInterestRate: faker.random.number({ 'min': 3, 'max': 6 })
            })
          })
        }
      })
    })
  }

  response.body = data
  // response.body = []
  // response.header.returnCode = 'M9800'
  // response.header.returnMessage = 'M98　測試看看嘍！'
  setTimeout(() => res.json(response), 3000)
})

// 分期金額試算表
router.post('/01/ccd070103', (req, res, next) => {
  const response = createResponse('CCD070103')
  response.header.sessionID = req.body.header.sessionID
  const months = parseInt(req.body.body.planId.replace(/[^\d.]/g, '')) // 因為只傳入planId取試算表，為方便測試將planId與months設相同
  const totalAmount = faker.random.number({ 'min': 10000, 'max': 20000 })
  const fixedAmount = parseInt(totalAmount / months)
  const data = {
    fixedAmount: fixedAmount.toString(),
    totalAmount: totalAmount.toString(),
    planId: req.body.body.planId,
    months: months.toString(),
    sbiInterestRate: (faker.random.number({ 'min': 100, 'max': 200 }) / 100).toString()

  }
  response.body = data
  // response.header.returnCode = 'M9801'
  // response.header.returnMessage = 'I hate you....'
  setTimeout(() => res.json(response), 3000)
})

// 新增單筆分期
router.post('/01/ccd070104', (req, res, next) => {
  const response = createResponse('CCD070104')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  response.header.returnMessage = '成功變更'
  res.json(response)
})

// 設定自動分期
router.post('/02/ccd070201', (req, res, next) => {
  const response = createResponse('CCD070201')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  response.header.returnMessage = '成功變更'
  res.json(response)
})

// 查詢自動分期設定
router.post('/02/ccd070202', (req, res, next) => {
  const response = createResponse('ccd070202')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const sbiInterestRate = faker.random.number({ min: 0, max: 10, precision: 0.01 })
  const data = {
    indicator: "1",
    sbiInterestRate: sbiInterestRate.toString(),
    tenureList: [{ tenure: "3" }, { tenure: "24" }],
    thresholdList: [{ threshold: "1000" }, { threshold: "2000" }, { threshold: "3000" }],
    instalmentTenure: "3",
    thresholdAmount: "3000",
    // instalmentTenure: null
    // thresholdAmount: null,
    interestRate: "888",
    sbiPlan: "999"

  }
  response.body = data
  res.json(response)
})

// 查詢分期歷史記錄
router.post('/03/ccd070301', (req, res, next) => {
  const response = createResponse('CCD070301')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  var dateArray = ['20170301', '20170302', '20170303', '20170304', '20170305', '20170306', '20170307', '20170308', '20170309', '20170310', '20170311', '20170312', '20170313', '20170314']
  const instalmentList = Duplicate.map(RandomArray.get([5, 10]), () => {
    return {
      instalmentChannel: RandomArray.get(['網路', 'APP', '電話']),
      instalmentCreateDate: RandomArray.get(dateArray),
      totalAmount: faker.finance.amount(100, 10000, 0).toString(),
      months: RandomArray.get(['3', '6', '12', '18', '24', '32']),
      instalmentStatus: ''
    }
  })
  response.body = { instalmentList }
  // response.header.returnCode = 'M980111-S001'
  // response.header.returnMessage = 'What the fuck......'
  setTimeout(() => res.json(response), 5000)
})

// 新增已出帳單分期
router.post('/04/ccd070401', (req, res, next) => {
  const response = createResponse('CCD070401')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  res.json(response)
})

// 已出帳單分期金額試算表
router.post('/04/ccd070402', (req, res, next) => {
  const response = createResponse('CCD070402')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'

  response.body = {
    fixedAmount: 43000,
    planId: 'plan-3',
    months: 3,
    interestRate: '10.0',
    ccNo: 1123581311235813

  }

  //setTimeout(() => res.json(response), 5000)
  res.json(response)
})

// 查詢已出帳單可承作分期(SSBI)
router.post('/04/ccd070403', (req, res, next) => {
  const response = createResponse('CCD070403')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'

  const planList = ['3', '6', '9', '12', '24', '30'].map((months) => {
    return ({
      planId: 'plan-' + months,  // 為方便測試將planId與months設相同
      months: months,
      sbiInterestRate: faker.random.number({ 'min': 3, 'max': 6 })
    })
  })
  response.body = {
    planList,
    maxSSBIAmount: 43000,
    totalPendingSSBIAmount: 33000
  }

  setTimeout(() => res.json(response), 1000)
})










// =======================
// NON ETC Personal Loan
// =======================

// CCD070512 個人信貸申請-NTC基本資料
router.post('/05/ccd070512', (req, res, next) => {
  const response = createResponse('CCD070512')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {
    doctype: `2`, // string | M | 進件類型1: ETC 2: NTC
    queryButton: `1`, // string | M | 查詢申請進度按鈕 1:顯示0:不顯示
    continueButton: `0`, // string | M | 繼續申請按鈕 1:顯示0:不顯示

    isEtb: `1`, // string | O | ETB:1 ，NTB:0
    etbAcct: [`1234567`, `4567890`], // String[] | O | ETB銀行帳戶清單
    etbMobile: `0929797587`, // string | O | ETB手機號碼

    extension: '1234567', // opt extension
    caseTmp: 'TLyyyymmdd12345' // temp case no
  }
  response.body = data
  res.json(response)
})


// CCD070513 個人信貸申請-FISC身分驗證
router.post('/05/ccd070513', (req, res, next) => {
  const response = createResponse('CCD070513')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {
    extension: '1234567', // string | O | 銀行手機號碼（加密）未登入此欄位為必填
  }
  response.body = data
  res.json(response)
})


// CCD070505 個人信貸申請-儲存貸款資料
router.post('/05/ccd070505', (req, res, next) => {
  const response = createResponse('CCD070505')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {}
  response.body = data
  res.json(response)
})


// CCD070508 個人信貸申請-上傳檔案
router.post('/05/ccd070508', (req, res, next) => {
  const {
    body: {
      body: {
        fileType,
        pic,
        picExtension
      }
    }
  } = req

  const r = createResponse('CCD070508')
  r.header.sessionID = req.body.header.sessionID

  if (pic && pic.length > 0) {
    r.body = { fileName: `f-${fileType}.${picExtension}` }
  } else {
    r.body = { fileName: '' }
  }

  // r.header.returnCode = 'M2200'
  // r.header.returnMessage = '上傳失敗'
  setTimeout(
    () => res.json(r),
    300
  )
})


// CCD070506 個人信貸申請-取得身分證OCR
router.post('/05/ccd070506', (req, res, next) => {
  const response = createResponse('CCD070506')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {
    issuePlace: '10008000', // String | M | 發證地點
    issueType: '2', // String | M | 發證型態 1: "初發", 2:  "補發", 3: "換發"
    issueDate: '2000-03-25', // String | M | 發證日期 yyyy-mm-dd
    birthday: '1983-03-25', // String | M | 身分證生日 yyyy-mm-dd
    householdZip: '234', // String | M | 戶籍地ZIP (需依照鄉鎮市區判斷出後回傳)
    householdCity: 'NO_USED=>新北市', // String | M | 戶籍地縣市
    householdArea: 'NO_USED=>永和區', // String | M | 戶籍地鄉鎮市區
    householdAddr: '福和路152號 1234123123', // String | M | 戶籍地址
  }
  response.body = data
  res.json(response)
})


// CCD070507 個人信貸申請-儲存個人及公司資訊
router.post('/05/ccd070507', (req, res, next) => {
  const response = createResponse('CCD070507')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {

  }
  response.body = data
  res.json(response)
})

// CCD070510 個人信貸申請-儲存撥款帳戶資訊
router.post('/05/ccd070510', (req, res, next) => {
  const response = createResponse('CCD070510')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {

  }
  response.body = data
  res.json(response)
})


// CCD070511 個人信貸申請-確認資訊
router.post('/05/ccd070511', (req, res, next) => {
  const response = createResponse('CCD070511')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {

  }
  response.body = data
  res.json(response)
})


// CCD070516 個人信貸申請-下載已上傳檔案 
router.post('/05/ccd070516', (req, res, next) => {
  const r = createResponse('CCD070516')
  r.header.sessionID = req.body.header.sessionID

  r.body = {
    uploadFile: 'R0lGODlhCgAKAPABAAAAAP///yH5BAHoAwEALAAAAAAKAAoAAAIUjI8HC9kKA5OmvYov3fNqLU3JGBQAOw=='
  }

  res.json(r)
})


// CCD070517 個人信貸申請-儲存申請資料
router.post('/05/ccd070517', (req, res, next) => {
  const response = createResponse('CCD070517')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {

  }
  response.body = data
  res.json(response)
})

const isEtb = true
const isFisc = true // 使用聯徵帳號做身分驗證
const isNotEtbSameAsOtherBank = isFisc && true // 使用聯徵帳號作為撥款帳號

// CCD070502 個人信貸申請-繼續申請
router.post('/05/ccd070502', (req, res, next) => {
  const response = createResponse('CCD070502')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {
    caseNo: 'PLyyyymmdd12345', // String | M | 案件編號
    custId: 'Z299900093', // String | M | 身份證字號
    custCname: '周杰倫', // String | M | 中文姓名
    custMobile: '0911222333', // String | M | 手機號碼
    custEmail: 'temp@gmail.com', // String | M | 電子信箱
    createDate: '', // String | M | 建立日期 YYYYMMDD HH:mm:ss
    doctype: '2', // String | M | 進件類型 1: ETC, 2: NTC
    productType: '', // String | M | 貸款類型
    etcAppStep: '', // String | O | ETC APP申請步驟
    etcWebStep: '', // String | O | ETC WEB申請步驟
    netcWebStep: '23', // String | O | NETC WEB申請步驟 10: 貸款資料頁, 21: 身分證上傳頁, 22: 個人資訊頁, 23: 公司資訊頁, 24: 通訊地址頁, 31: 上傳財力證明文件, 32: 撥款帳戶資訊, 40: 確認資料頁, 50: 完成申請
    status: '1', // String | M | 申請進度 1:繼續申請, 2:待審核, 3:審核中, 4:專人聯絡, 5:核准, 6:已撥款
    expiryDate: '', // String | M | 申請截止日YYYYMMDD
    modifyDate: '', // String | M | 更新日期 YYYYMMDD HH:mm:ss
    submitDate: '', // String | O | 送交IWF日期 YYYYMMDD
    birthday: '1980-01-01', // String | O | 生日 YYYY-MM-DD
    eduLevel: '5', // String | O | 最高學歷 1:博士 2:碩士 3:大學 4:專科 5:高中職 6:其他
    marriStatus: '', // String | O | 婚姻狀況 S:未婚 M:已婚 D:其他
    issuePlace: '10008000', // String | O | 身分證發證地點，7~8碼，代碼請參見CCOA
    issueType: '3', // String | O | 身分證發證類型 1: "初發", 2:  "補發", 3: "換發"
    issueDate: '2018-12-31', // String | O | 身分證發證日期YYYY-MM-DD
    residentStatus: '4', // String | O | 現居狀態 1:自有; 2:親屬; 3:租賃; 4:宿舍; 5:其他
    customerSegment: '', // String | O | 客層 M: M LOAN, Z: EASY LOAN , 空白: NTC
    householdZip: '234', // String | O | 戶籍郵遞區號 先抓CCDS_PLA_MAIN沒有資料則帶V+
    householdCity: '新北市', // String | O | 戶籍縣市 先抓CCDS_PLA_MAIN沒有資料則帶V+
    householdArea: '永和區', // String | O | 戶籍區域 先抓CCDS_PLA_MAIN沒有資料則帶V+
    householdAddr: '福和路152號 1234123123 56789 qweqwoieuqwepoiu 1iosdf', // String | O | 戶籍填寫地址 先抓CCDS_PLA_MAIN沒有資料則帶V+
    fullHouseholdAddr: '', // String | O | 戶籍完整地址 先抓CCDS_PLA_MAIN沒有資料則帶V+
    householdTel: '', // String | O | 戶籍電話號碼
    isHHresidental: '0', // String | O | 是否使用戶籍地址為現居地址   !!!!!!!!!!!!!!isHouseholdResidental?!!!!!!!!!
    residentZip: '234', // String | O | 現居郵遞區號
    residentCity: '新北市', // String | O | 現居縣市
    residentArea: '永和區', // String | O | 現居區域
    residentAddr: '就在這邊', // String | O | 現居填寫地址
    fullResidentAddr: '', // String | O | 現居完整地址
    residentTel: '', // String | O | 住家電話號碼
    businessCode: '1200', // String | O | 行業別
    occCode: '35', // String | O | 職業別
    companyNm: '這間公司名稱', // String | O | 公司名稱
    workYm: '0001', // String | O | 年資YYMM
    companyDept: '部門', // String | O | 部門
    jobTitle: '職稱', // String | O | 職稱
    annualIncome: '99', // String | O | 年收入 (萬元)
    preCompanyYm: '前公司名稱', // String | O | 前職公司名稱 !!!!!!!!Ym?!!!!!!!!!!!!!!!
    preJobTitle: '前職稱', // String | O | 前職職稱
    preWorkYm: '0211', // String | O | 前職工作年資
    sameJob: '', // String | O | 是否同公司 1:是 0:否
    jobChangeReason: '', // String | O | 職業異動原因 1:職業轉換 2:退休 3:離職 4:自營 5:其他
    companyZip: '434', // String | O | 公司郵遞區號 先抓CCDS_PLA_MAIN沒有資料則帶V+
    companyCity: '臺中市', // String | O | 公司縣市 先抓CCDS_PLA_MAIN沒有資料則帶V+
    companyArea: '龍井區', // String | O | 公司區域 先抓CCDS_PLA_MAIN沒有資料則帶V+
    companyAddr: '前公司地址', // String | O | 公司地址 先抓CCDS_PLA_MAIN沒有資料則帶V+
    fullCompanyAddr: '', // String | O | 公司完整地址 先抓CCDS_PLA_MAIN沒有資料則帶V+
    companyTell: '02-27714944', // String | O | 公司電話
    billDeliveryType: '4', // String | O | 聯絡地址選項 1:同現居地址; 2: 同戶籍地址; 3: 同公司地址 4:其他
    otherZIp: '434', // String | O | 其他郵遞區號
    otherCity: '臺中市', // String | O | 其他縣市
    otherArea: '龍井區', // String | O | 其他區域
    otherAddr: '其他地址', // String | O | 其他填寫地址
    relativeCname: '關係人', // String | O | 同一關係人姓名
    relativeRelationship: '14', // String | O | 同一關係人關係 1.本人 2.配偶 3.祖父 4.祖母 5.父 6.母 7.子 8.女 9.孫子 10.孫女 11.兄 12.弟 13.姊 14.妹
    relatvieCustId: 'Z299900093', // String | O | 同一關係人身分證字號
    cddRisk: '', // String | O | CDD職業風險
    loanAmt: '666666666', // String | O | 貸款申請金額
    loanTendor: '7', // String | O | 貸款申請期間(年)
    loanPurpose: '5', // String | O | 貸款用途 1:償還借款 2:居家修繕 3:消費支出 4:投資理財 5:其他
    loanPurposeOther: '蓋大樓', // String | O | 貸款其他用途
    preId: '', // String | O | 預審信貸序號
    eligid: '', // String | O | 信貸資格序號
    ezLoanPriceOffer: '', // String | O | 貸款利率 (EZL only)
    loanPlm: '', // String | O | 月付金 (EZL only)
    loadFee: '', // String | O | 手續費 (EZL only)
    jSegment: '', // String | O | J segment
    disbursementBankId: '099', // String | O | 撥款銀行Id
    disbursementBranchName: '這個分行', // String | O | 撥款分行名稱
    disbursementAcct: '22611316', // String | O | 撥款銀行帳號
    salesCode: '', // String | O | Sales code
    salesEmail: '', // String | O | Sales Email
    dataChangeRemark: '', // String | O | Remark from Card+ for data change alert
    mediaCode: '', // String | O | Media code 截取referralCode=後的value
    nidFileD1: 'a1-file-name.png', // String | O | 身分證正面檔名
    d1Size: '', // String | O | 身分證正面檔案大小
    nidFileD2: 'a2-file-name.png', // String | O | 身分證背面檔名
    d2Size: '', // String | O | 身分證背面檔案大小
    incomeProfB1: 'b1-file-name.png', // String | O | 財力證明文件檔名1
    b1Size: '', // String | O | 財力證明文件1檔案大小
    incomeProfB2: 'b2-file-name.png', // String | O | 財力證明文件檔名2
    b2Size: '', // String | O | 財力證明文件2檔案大小
    incomeProfB3: '', // String | O | 財力證明文件檔名3
    b3Size: '', // String | O | 財力證明文件3檔案大小
    incomeProfB4: '', // String | O | 財力證明文件檔名4
    b4Size: '', // String | O | 財力證明文件4檔案大小
    incomeProfB5: '', // String | O | 財力證明文件檔名5
    b5Size: '', // String | O | 財力證明文件5檔案大小
    incomeProfB6: '', // String | O | 財力證明文件檔名6
    b6Size: '', // String | O | 財力證明文件6檔案大小
    incomeProfB7: '', // String | O | 財力證明文件檔名7
    b7Size: '', // String | O | 財力證明文件7檔案大小
    incomeProfB8: '', // String | O | 財力證明文件檔名8
    b8Size: '', // String | O | 財力證明文件8檔案大小
    incomeProfB9: '', // String | O | 財力證明文件檔名9
    b9Size: '', // String | O | 財力證明文件9檔案大小
    incomeProfB10: '', // String | O | 財力證明文件檔名10
    b10Size: '', // String | O | 財力證明文件10檔案大小
    incomeProfB11: '', // String | O | 財力證明文件檔名11
    b11Size: '', // String | O | 財力證明文件11檔案大小
    incomeProfB12: '', // String | O | 財力證明文件檔名12
    b12Size: '', // String | O | 財力證明文件12檔案大小
    incomeProfB13: '', // String | O | 財力證明文件檔名13
    b13Size: '', // String | O | 財力證明文件13檔案大小
    incomeProfB14: '', // String | O | 財力證明文件檔名14
    b14Size: '', // String | O | 財力證明文件14檔案大小
    incomeProfB15: '', // String | O | 財力證明文件檔名15
    b15Size: '', // String | O | 財力證明文件15檔案大小
    appForm: '', // String | O | 申請書PDF
    appFormSize: '', // String | O | 申請書size
    smtFollowDate: '', // String | O | 送出補件日期 YYYYMMDD
    noticeDate: '', // String | O | 提醒通知日期 YYYYMMDD
    isAgreeTC: '', // String | O | 是否同意個人信貸條款
    disclaimerETCTc: '', // String | O | ETC信貸聲明與注意事項URL
    disclaimerNETCTc: 'http://www.princexml.com/samples/flyer/flyer.pdf', // String | O | NETC信貸聲明與注意事項URL
    isAgreeRepayment: '', // String | O | 是否同意還款條款
    agreeRepayment: '', // String | O | 同意還款條款
    iwfCaseId: '', // String | O | CFTW 回傳的IWF Case ID
    iwfSaveStatus: '', // String | O | IWF回傳儲存成功註記 0:fail, 1:success'
    leapsCaseId: '', // String | O | CFTW 回傳的LEAPS Case ID
    isFisc: isFisc ? '1' : '0', // String | O | 是否聯徵Y/N
    fiscUpdateDate: '', // String | O | FISC 驗證日期 YYYYMMDD HH:mm:ss
    isEtb: isEtb ? '1' : '0', // String | O | 是否ETB Y/N
    changeRemark: '發證日期 生日', // String | O | 客戶異動欄位紀錄
    fiscBankId: '099', // String | O | 身分驗證之銀行代碼
    fiscBankAcct: '9000', // String | O | 身分驗證之銀行帳號
    fiscBankCtry: '886', // String | O | 身分驗證之銀行國碼
    fiscBankMobile: '0988888888', // String | O | 身分驗證之手機號碼
    isHouseholdResidental: '0', // string | O | 是否使用戶籍地址為現居地址 1: 是  0: 不是
    isUseFiscAcct: (isFisc && isNotEtbSameAsOtherBank) ? '1' : '0', // String | M | 是否使用聯徵帳號為撥款帳號
    etbAcct: isEtb ? [`1234567`, `4567890`] : [], // String[] | O | ETB銀行帳戶清單
    etbMobile: isEtb ? `0929797587` : '' // string | O | ETB手機號碼
  }
  response.body = data
  res.json(response)
})

// CCD070518 個人信貸申請-NTC查詢登入 
router.post('/05/ccd070518', (req, res, next) => {
  const response = createResponse('CCD070518')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {
    extension: 'exteionsyoyoyoy'
  }
  response.body = data
  res.json(response)
})


// CCD070514 個人信貸申請-申請進度查詢
router.post('/05/ccd070514', (req, res, next) => {
  const response = createResponse('CCD070514')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {
    docType: '0', // 1:ETC 0:NTC
    caseList: [
      {
        caseNo: 'LP0001', // string | O | 案件編號
        modifyDate: moment().add(-5, 'days').format('YYYY-MM-DD HH:mm'), // string | O | yyyy-mm-dd hr12:mm
        expiryDate: moment().add(1, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        submitDate: moment().add(1, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        isUpload: '0', // string | O | 1:顯示上傳文件 0:不顯示上傳文件
        status: '1', // string | O | 案件狀態 1:繼續申請, 2:待審核, 3:審核中, 4:專人聯絡, 5:核准, 6:已撥款
        incomeProfB11: '', // String | O | 財力證明文件檔名11
        incomeProfB12: '', // String | O | 財力證明文件檔名12
        incomeProfB13: '', // String | O | 財力證明文件檔名13
        incomeProfB14: '', // String | O | 財力證明文件檔名14
        incomeProfB15: '', // String | O | 財力證明文件檔名15
      },
      {
        caseNo: 'LP0002', // string | O | 案件編號
        modifyDate: moment().add(-4, 'days').format('YYYY-MM-DD HH:mm'), // string | O | yyyy-mm-dd hr12:mm
        expiryDate: moment().add(2, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        submitDate: moment().add(1, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        isUpload: '1', // string | O | 1:顯示上傳文件 0:不顯示上傳文件
        status: '2', // string | O | 案件狀態 1:繼續申請, 2:待審核, 3:審核中, 4:專人聯絡, 5:核准, 6:已撥款
        incomeProfB11: '', // String | O | 財力證明文件檔名11
        incomeProfB12: '', // String | O | 財力證明文件檔名12
        incomeProfB13: '', // String | O | 財力證明文件檔名13
        incomeProfB14: '', // String | O | 財力證明文件檔名14
        incomeProfB15: '', // String | O | 財力證明文件檔名15
      },
      {
        caseNo: 'LP0003', // string | O | 案件編號
        modifyDate: moment().add(-4, 'days').format('YYYY-MM-DD HH:mm'), // string | O | yyyy-mm-dd hr12:mm
        expiryDate: moment().add(2, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        submitDate: moment().add(1, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        isUpload: '1', // string | O | 1:顯示上傳文件 0:不顯示上傳文件
        status: '2', // string | O | 案件狀態 1:繼續申請, 2:待審核, 3:審核中, 4:專人聯絡, 5:核准, 6:已撥款
        incomeProfB11: 'fileName1.png', // String | O | 財力證明文件檔名11
        incomeProfB12: 'fileName2.png', // String | O | 財力證明文件檔名12
        incomeProfB13: '', // String | O | 財力證明文件檔名13
        incomeProfB14: '', // String | O | 財力證明文件檔名14
        incomeProfB15: '', // String | O | 財力證明文件檔名15
      },
      {
        caseNo: 'LP0004', // string | O | 案件編號
        modifyDate: moment().add(-4, 'days').format('YYYY-MM-DD HH:mm'), // string | O | yyyy-mm-dd hr12:mm
        expiryDate: moment().add(2, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        submitDate: moment().add(1, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        isUpload: '0', // string | O | 1:顯示上傳文件 0:不顯示上傳文件
        status: '3', // string | O | 案件狀態 1:繼續申請, 2:待審核, 3:審核中, 4:專人聯絡, 5:核准, 6:已撥款
        incomeProfB11: '', // String | O | 財力證明文件檔名11
        incomeProfB12: '', // String | O | 財力證明文件檔名12
        incomeProfB13: '', // String | O | 財力證明文件檔名13
        incomeProfB14: '', // String | O | 財力證明文件檔名14
        incomeProfB15: '', // String | O | 財力證明文件檔名15
      },
      {
        caseNo: 'LP0005', // string | O | 案件編號
        modifyDate: moment().add(-4, 'days').format('YYYY-MM-DD HH:mm'), // string | O | yyyy-mm-dd hr12:mm
        expiryDate: moment().add(2, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        submitDate: moment().add(1, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        isUpload: '0', // string | O | 1:顯示上傳文件 0:不顯示上傳文件
        status: '4', // string | O | 案件狀態 1:繼續申請, 2:待審核, 3:審核中, 4:專人聯絡, 5:核准, 6:已撥款
        incomeProfB11: '', // String | O | 財力證明文件檔名11
        incomeProfB12: '', // String | O | 財力證明文件檔名12
        incomeProfB13: '', // String | O | 財力證明文件檔名13
        incomeProfB14: '', // String | O | 財力證明文件檔名14
        incomeProfB15: '', // String | O | 財力證明文件檔名15
      },
      {
        caseNo: 'LP0006', // string | O | 案件編號
        modifyDate: moment().add(-4, 'days').format('YYYY-MM-DD HH:mm'), // string | O | yyyy-mm-dd hr12:mm
        expiryDate: moment().add(2, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        submitDate: moment().add(1, 'days').format('YYYY-MM-DD'), // string | O | YYYY-MM-DD
        isUpload: '0', // string | O | 1:顯示上傳文件 0:不顯示上傳文件
        status: '5', // string | O | 案件狀態 1:繼續申請, 2:待審核, 3:審核中, 4:專人聯絡, 5:核准, 6:已撥款
        incomeProfB11: '', // String | O | 財力證明文件檔名11
        incomeProfB12: '', // String | O | 財力證明文件檔名12
        incomeProfB13: '', // String | O | 財力證明文件檔名13
        incomeProfB14: '', // String | O | 財力證明文件檔名14
        incomeProfB15: '', // String | O | 財力證明文件檔名15
      }
    ]
  }
  response.body = data
  res.json(response)
})


// CCD070515 個人信貸申請-取消信貸申請 
router.post('/05/ccd070515', (req, res, next) => {
  const response = createResponse('CCD070515')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {
  }
  response.body = data
  res.json(response)
})


// CCD070519 個人信貸申請-補件上傳
router.post('/05/ccd070519', (req, res, next) => {
  const response = createResponse('CCD070519')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  const data = {

  }
  response.body = data
  res.json(response)
})


module.exports = router
