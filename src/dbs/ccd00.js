const express = require('express')
const router = express.Router()
const faker = require('faker')
const path = require('path')
const fs = require('fs')
const RandomArray = require('../dbs-lib/FnRandomArray.js')
const createResponse = require('../dbs-lib/common-response').createResponse
const Duplicate = require('../dbs-lib/FnDuplicate.js')

faker.locale = 'zh_TW'

const content = `
星展(台灣)商業銀行股份有限公司（以下稱「星展銀行」）所發佈之資訊僅供參
考之用......。`
// 取得驗證碼
router.post('/01/ccd000101', (req, res, next) => {
  const response = createResponse('CCD000101')
  response.header.sessionID = req.body.header.sessionID
  const seed = faker.internet.password(4)
  const image = `http://image.captchas.net/?client=demo&random=${seed}&width=150&height=40`
  const data = {
    caCodeImg: `${image}`,
    caID: seed
  }
  response.body = data

  res.json(response)
})

// 發送OTP/重新發送OTP
router.post('/01/ccd000102', (req, res, next) => {
  const response = createResponse('CCD000102')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    opaque: faker.internet.password(8),
    phoneNumber: faker.phone.phoneNumber()
  }
  response.body = data

  setTimeout(() => res.json(response), 2000)
})

// 驗證OTP
router.post('/01/ccd000103', (req, res, next) => {
  const response = createResponse('CCD000103')
  response.header.sessionID = req.body.header.sessionID
  // response.header.returnCode = 'M9999'
  // response.header.returnMessage = '測試用失敗'
  res.json(response)
})

// 取得條款
router.post('/01/ccd000104', (req, res, next) => {
  const response = createResponse('CCD000104')
  response.header.sessionID = req.body.header.sessionID

  const pdfUrlSamples = [
    'http://www.princexml.com/samples/invoice/invoicesample.pdf',
    'http://www.princexml.com/howcome/2016/samples/invoice/index.pdf',
    'http://www.princexml.com/howcome/2016/samples/magic8/index.pdf',
    'http://www.princexml.com/samples/flyer/flyer.pdf'
  ]

  const data = {
    // tcVersionSEQ: 'v01'// 條款table序號
    termURL: RandomArray.get(pdfUrlSamples) // 條款連結 (PWEB URL)
  }
  if (req.body.body.termType === 'A06') {
    data.termURL = 'http://localhost:3300/LSP%20RSP%20Subscription_ch.htm'
  }
  if (req.body.body.termType === 'Z01') {
    data.termURL = 'https://www.dbs.com.tw/treasures-zh/digiservice/2017Q4/index.html?pid=TW_eStmt'
  }

  response.body = data
  res.json(response)
})

// 取得注意事項
router.post('/01/ccd000105', (req, res, next) => {
  const response = createResponse('CCD000105')
  response.header.sessionID = req.body.header.sessionID

  const data = {
    conStart: '免責聲明與重要注意事項' + req.body.body.noteType,
    conEnd: '本資訊無意散布給任何在其所屬管轄地或國家就本資訊之散布或使用可能違反法律或規則之個人或法律實體，亦不供其使用。',
    noteList: Duplicate.map(faker.random.number({ 'min': 1, 'max': 6 }),
      () => ({ content: content })
    )
  }
  if (req.body.body.noteType === 'M01') {
    data.noteList = [{ content: '若未如期繳納帳單最低應繳金額，此筆交易將被取消' }]
  }
  if (req.body.body.noteType === 'M04') {
    data.noteList = [{ content: '提醒您，您已完成帳單分期申請，但帳單分期仍須符合以下條件才能辦理成功：1。需繳足帳單最低應付金額，若未如期繳納，此筆帳單分期交易將被取消。 2.持卡人卡片狀態不得有延遲繳款，停卡或違反本行信用卡約定條款之情事。' }]
  }

  if (req.body.body.noteType === 'A01') {
    data.conStart = '您的開卡已經完成囉，只差最後一個步驟就可以完成星展Card+信用卡數位服務註冊，首次登入星展Card+信用卡行動服務還贈送您7-11 100元禮券'
  }

  if (req.body.body.noteType === 'M03') {
    data.conStart = '自動分期：一經申請設定且經本行核准者，自設定日起，申請人之本行信用卡正附卡單筆新增之消費總金額達其指定之單筆分期交易最低限額時，該筆消費將依指定期數進行分期。'
  }
  if (req.body.body.noteType === 'M05') {
    data.conStart = '您目前尚未申請自動分期，欲申請請按下方按鈕依指示完成設定。'
  }
  response.body = data
  res.json(response)
})

// 儲存暫存資料
router.post('/01/ccd000106', (req, res, next) => {
  const response = createResponse('CCD000106')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 取得暫存資料
router.post('/01/ccd000107', (req, res, next) => {
  const response = createResponse('CCD000107')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    attName: req.body.body.attName,
    attValue: faker.random.words()
  }
  response.body = data
  res.json(response)
})

// 取得認證章
router.post('/01/ccd000108', (req, res, next) => {
  const response = createResponse('CCD000108')
  response.header.returnCode = 'M0000'
  response.header.returnMessage = ''
  response.header.sessionID = faker.internet.password(20)
  res.json(response)
})

// 系統維護公告
router.post('/01/ccd000109', (req, res, next) => {
  const response = createResponse('CCD000109')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    content: faker.lorem.words(faker.random.number({ 'min': 20, 'max': 40 }))
  }
  response.body = data
  res.json(response)
})

// 系統設定查詢
router.post('/01/ccd000110', (req, res, next) => {
  const response = createResponse('CCD000110')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    paraList: []
  }

  // 當客戶未申請電子帳單時，提供給客戶的連結位置
  data.paraList.push({
    paraName: 'APPLY_ELE_BILL',
    paraValue: 'https://estatement.dbs.com.tw/'
  })
  // 不可註冊與登入的註記
  data.paraList.push({
    paraName: 'BLOCK_CODE_LIMIT',
    paraValue: 'Z,Y'
  })
  // 更多信用卡優惠 URL
  data.paraList.push({
    paraName: 'CARD_OFFER',
    paraValue: 'https://www.dbs.com.tw/cardapply'
  })
  // 常見問題URL
  data.paraList.push({
    paraName: 'COMMON_QUES_URL',
    paraValue: 'http://www.dbs.com.tw/personal-zh/Footer/FAQ.page'
  })
  // 設定客戶於App點選聯絡星展的電話號碼
  data.paraList.push({
    paraName: 'CONTACT_DBS_PHONE',
    paraValue: '02-66129889'
  })
  // 前台用戶閒置時間過長時，在用戶被自動登出前，會跳出一個提醒視窗進行被登出的倒數計時，可在此設定提醒視窗倒數計時的秒數(目前為300秒，5分鐘)
  data.paraList.push({
    paraName: 'COUNT_DOWN_SEC',
    paraValue: '300'
  })
  // 設定客戶SMS OTP的有效秒數
  data.paraList.push({
    paraName: 'EFFECTIVE_TIME_SET',
    paraValue: '100'
  })
  // 設定登入頁右下方臉書icon的連結位置
  data.paraList.push({
    paraName: 'FB_URL',
    paraValue: 'https://www.facebook.com/dbs.tw/'
  })
  // 客戶閒置超過此時間將被自動登出
  data.paraList.push({
    paraName: 'FORCE_LOGOUT_TIME',
    paraValue: '600'
  })
  // 設定紅利兌換的外部連結網址
  data.paraList.push({
    paraName: 'GO_POWER_URL',
    paraValue: 'https://www.gopower.com.tw/dbs'
  })
  // IB 登入頁URL
  data.paraList.push({
    paraName: 'IB_LOGIN_URL',
    paraValue: 'https://10.91.35.232:60026/Banking/retail/logon/ccds'
  })
  // IB產品驗證頁URL
  data.paraList.push({
    paraName: 'IB_PRODUCT_URL',
    paraValue: 'https://10.91.35.232:60026/Banking/retail/logon/ccds'
  })
  // 無效卡註記
  data.paraList.push({
    paraName: 'INACTIVE_CARD_CODE',
    paraValue: 'A,C,D,E,F,G,H,J,N,O,R,T,V'
  })
  // Line URL
  data.paraList.push({
    paraName: 'LINE_URL',
    paraValue: 'https://line.me/R/ti/p/%40dbstw'
  })
  // Login customer limit
  data.paraList.push({
    paraName: 'LOGIN_LIMIT',
    paraValue: '400'
  })
  // 設定加辦信用卡功能的單次最高可申請卡片數量
  data.paraList.push({
    paraName: 'MAX_APPLY_CARD',
    paraValue: '3'
  })
  // 需要協助URL
  data.paraList.push({
    paraName: 'NEED_HELP_URL',
    paraValue: 'http://www.dbs.com.tw/personal-zh/Footer/FAQ.page'
  })
  // 設定客戶作其他銀行帳戶繳款時，本行所收取的手續費費用
  data.paraList.push({
    paraName: 'OTHER_BANK_FEE',
    paraValue: '0'
  })
  // PWEB 查卡API URL
  data.paraList.push({
    paraName: 'PWEB_API_URL',
    paraValue: 'http://localhost:3300/api/cards'
  })
  // PWEB 查卡API URL
  data.paraList.push({
    paraName: 'PWEB_API_URL_NEW',
    paraValue: 'http://localhost:3300/api/cards-new'
    // paraValue: 'https://www.dbs.com.tw/twproductsvc/v1/contentapi/flpstore_main_www_tw_personal_mmcontent_mmproductdetail/search'
  })
  // PWEB申請IB 產品URL
  data.paraList.push({
    paraName: 'PWEB_APPLYIB_URL',
    paraValue: 'https://www.dbs.com.tw/cardapply'
  })
  // 設定PWEB 上申請信用卡的完整連結網址
  data.paraList.push({
    paraName: 'PWEB_CARDAPPLY_URL',
    paraValue: 'https://www.dbs.com.tw/cardapply'
  })
  // 密碼未變更提醒期限(月)
  data.paraList.push({
    paraName: 'PW_NOCHANGE_REMIND',
    paraValue: '12'
  })
  // 設定客戶可重新發送SMS OTP的秒數
  data.paraList.push({
    paraName: 'RESEND_TIME_SET',
    paraValue: '10'
  })
  // 分期申辦最低金額
  data.paraList.push({
    paraName: 'TRANS_INSTALLMENT_LOWAMOUNT',
    paraValue: '3000'
  })
  // 前台登入時，客戶超過3個月未登入視為inactive，僅提供Report04.Card+ EOP_Monthly-Usage Status 使用
  data.paraList.push({
    paraName: 'UNREGI_OMNISYS_TIME',
    paraValue: '3'
  })
  // 設定登入頁右下方Yotube icon的連結位置
  data.paraList.push({
    paraName: 'YOUTUBE_URL',
    paraValue: 'https://www.youtube.com/channel/UCwAsuxe7JKNbCkT9BOTS1xw'
  })
  // 版權 URL
  data.paraList.push({
    paraName: 'COPY_RIGHT',
    paraValue: 'http://www.dbs.com.tw/personal-zh/default.page'
  })
  // 隱私權 URL
  data.paraList.push({
    paraName: 'PRIVACY_STATEMENT',
    paraValue: 'https://www.dbs.com/taiwan-zh/privacy.page'
  })
  // 使用條款 URL
  data.paraList.push({
    paraName: 'USE_TERM_URL',
    paraValue: 'https://www.dbs.com/taiwan-zh/terms.page'
  })
  // 可申請卡片
  data.paraList.push({
    paraName: 'APPLY_CARD',
    paraValue: '100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 240, 181'
  })
  // 分期固定期數
  data.paraList.push({
    paraName: 'SBI_MONTH',
    paraValue: '3,6,9,18,24, 36'
  })
  // Logger on AWS URL
  data.paraList.push({
    paraName: 'AWS_WEB_URL',
    paraValue: 'https://uat-dashboard.digital-analytics.com.sg/analyticsIBTWWS/rest/trackEventLogger/addTrackEventGetLog'
  })

  // 分期固定期數
  data.paraList.push({
    paraName: 'USE_ANZ_REG',
    paraValue: '0'
  })
  // 分期最低金額
  data.paraList.push({
    paraName: 'TRANS_INSTALLMENT_LOWAMOUNT',
    paraValue: '5000'
  })

  // 開卡後結果頁URL
  data.paraList.push({
    paraName: 'MORE_APP_INFO',
    paraValue: 'https://www.google.com.tw'
  })

  // IS_PAID_INFO: 是否顯示首頁上方Hi 以下的2行+按鈕
  // 0 :不顯示
  // 1: 要顯示
  data.paraList.push({
    paraName: 'IS_PAID_INFO',
    paraValue: RandomArray.get(['0', '1'])
  })

  // NCCC_URL
  data.paraList.push({
    paraName: 'NCCC_URL',
    paraValue: 'http://www.google.com.tw#oster-nccc-url'
  })

  // CCAPP_COPMTL_URL
  data.paraList.push({
    paraName: 'CCAPP_COPMTL_URL',
    paraValue: 'http://www.google.com.tw#oster-complt-url'
  })
  // 姓名翻譯 GOV_LINK
  data.paraList.push({
    paraName: 'GOV_LINK',
    paraValue: 'https://www.boca.gov.tw/sp-natr-singleform-1.html'
  })
  //消費分期上限筆數
  data.paraList.push({
    paraName: 'MAX_INSTALLMENT_AMOUNT',
    paraValue: '3'
  })
  //自動分期刪除按鈕
  data.paraList.push({
    paraName: 'AUTO_INSTALLMENT_SWITCH',
    paraValue: 'N'
  })

  // ===================== 貸款 =====================

  //了解詳情
  data.paraList.push({
    paraName: 'PLOAN_MORE_DETAIL',
    paraValue: 'https://www.dbs.com.tw/personal-zh/loans/personal-loans/personal-loan'
  })

  //財力證明上傳範例
  data.paraList.push({
    paraName: 'PLOAN_INCOME_DOC_EXAMPLE',
    paraValue: 'https://www.dbs.com.tw/personal-zh/personal-loans/ploanplus/income_doc_example.page'
  })

  //使用條款
  data.paraList.push({
    paraName: 'PLOAN_TERMS',
    paraValue: 'https://www.dbs.com/taiwan-zh/terms.page'
  })

  // 隱私權聲明
  data.paraList.push({
    paraName: 'PLOAN_PRIVACY',
    paraValue: 'https://www.dbs.com/taiwan-zh/privacy.page'
  })

  // 申請進度查詢
  data.paraList.push({
    paraName: 'PLOAN_NETC_QUERY',
    paraValue: 'develop-van-PLoan'
  })

  // ©星展(台灣)商業銀行股份有限公司
  data.paraList.push({
    paraName: 'PLOAN_DEFAULT_PAGE',
    paraValue: 'https://www.dbs.com.tw/personal-zh/default.page'
  })

  // 線上表格填寫頁面(PWeb)
  data.paraList.push({
    paraName: 'PLOAN_PWEB_LOAN_HTML',
    paraValue: 'https://www.dbs.com.tw/personal-zh/loan/index.html'
  })

  // FISC驗證之銀行列表
  data.paraList.push({
    paraName: 'PLOAN_FISC_BANK_LIST',
    paraValue: 'https://www.dbs.com.tw/personal-zh/personal-loans/ploanplus/FISC.page'
  })

  // FISC驗證之銀行列表
  data.paraList.push({
    paraName: 'FISC_BANK_LIST',
    paraValue: 'https://www.dbs.com.tw/personal-zh/personal-loans/ploanplus/FISC.page'
  })

  // 無限卡清單
  data.paraList.push({
    paraName: 'RICH_CARD_LOGO',
    paraValue: '181,180,100'
  })

  // CCOA 發行日
  data.paraList.push({
    paraName: 'CCOA_RELEASE_DT',
    paraValue: '20191022'
  })

  response.body = data
  res.json(response)
})

// 統統功能列表
router.post('/01/ccd000111', (req, res, next) => {
  const delayResponse = () => {
    const response = createResponse('CCD000111')
    response.header.sessionID = req.body.header.sessionID
    const data = {
      'funcList': [
        { funcCode: 'ccd020000', funcName: '首頁', open: 'E', funcType: '0', funcParent: '', funcLink: '/ccd020000', funcSort: '1' },
        { funcCode: 'ccd000001', funcName: '帳單', open: 'E', funcType: '0', funcParent: '', funcLink: '/ccd000001', funcSort: '2' },
        { funcCode: 'ccd030000', funcName: '繳款', open: 'E', funcType: '0', funcParent: '', funcLink: '/ccd030000', funcSort: '3' },
        { funcCode: 'ccd040000', funcName: '分期申辦', open: 'E', funcType: '0', funcParent: '', funcLink: '/ccd040000', funcSort: '4' },
        { funcCode: 'ccd000003', funcName: '活動登錄', open: 'E', funcType: '0', funcParent: '', funcLink: '/ccd000003', funcSort: '5' },
        { funcCode: 'ccd000002', funcName: '個人化服務', open: 'E', funcType: '0', funcParent: '', funcLink: '/ccd000002', funcSort: '6' },
        { funcCode: 'ccd030300', funcName: '未出帳單', open: 'E', funcType: '1', funcParent: 'ccd000001', funcLink: '/ccd030300', funcSort: '1' },
        { funcCode: 'ccd030100', funcName: '本期帳單', open: 'E', funcType: '1', funcParent: 'ccd000001', funcLink: '/ccd030100', funcSort: '2' },
        { funcCode: 'ccd030200', funcName: '電子帳單', open: 'E', funcType: '1', funcParent: 'ccd000001', funcLink: '/ccd030200', funcSort: '3' },
        { funcCode: 'ccd050000', funcName: '加辦信用卡', open: 'E', funcType: '1', funcParent: 'ccd000002', funcLink: '/ccd050000', funcSort: '1' },
        { funcCode: 'ccd050100', funcName: '更新個人資料', open: 'E', funcType: '1', funcParent: 'ccd000002', funcLink: '/ccd050100', funcSort: '2' },
        { funcCode: 'ccd050400', funcName: '通知設定', open: 'E', funcType: '1', funcParent: 'ccd000002', funcLink: '/ccd050400', funcSort: '3' },
        { funcCode: 'ccd050600', funcName: '掛失信用卡', open: 'E', funcType: '1', funcParent: 'ccd000002', funcLink: '/ccd050600', funcSort: '4' },
        { funcCode: 'ccd060000', funcName: '活動登錄', open: 'E', funcType: '1', funcParent: 'ccd000003', funcLink: '/ccd060000', funcSort: '1' },
        { funcCode: 'ccd060100', funcName: '活動登錄紀錄', open: 'E', funcType: '1', funcParent: 'ccd000003', funcLink: '/ccd060100', funcSort: '2' },
        { funcCode: 'ccd040100', funcName: '分期申辦', open: 'E', funcType: '1', funcParent: 'ccd040000', funcLink: '/ccd040100', funcSort: '1' },
        { funcCode: 'ccd040200', funcName: '分期歷史紀錄', open: 'E', funcType: '1', funcParent: 'ccd040000', funcLink: '/ccd040200', funcSort: '2' }
      ]
    }

    // response.header.returnCode = 'M7878-587'
    // response.header.returnMessage = '嘿嘿嘿，功能列表有誤'

    response.body = data
    res.json(response)
  }

  setTimeout(delayResponse, 100)
})

// 驗證CAPTCHA
router.post('/01/ccd000112', (req, res, next) => {
  const response = createResponse('CCD000112')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 取得條款
router.post('/01/ccd000113', (req, res, next) => {
  fs.readFile(path.resolve(__dirname, '../assets/demoPdf.txt'), 'utf8', function (err, data) {
    const response = createResponse('CCD000113')
    response.header.sessionID = req.body.header.sessionID
    const resData = {
      content: (err) ? '' : data
    }
    response.body = resData
    res.json(response)
  })
})

// 取得一次性的交易序號
router.post('/01/ccd000114', (req, res, next) => {
  const response = createResponse('CCD000114')
  response.header.sessionID = req.body.header.sessionID
  response.body = { txToken: faker.internet.password(36) }
  res.json(response)
})

// 取得一次性的交易序號
router.post('/01/ccd000115', (req, res, next) => {
  const response = createResponse('CCD000115')
  response.header.sessionID = req.body.header.sessionID
  response.body = { txToken: faker.internet.password(36) }
  res.json(response)
})

// 特殊事件
router.post('/02/ccd000201', (req, res, next) => {
  console.info('Request Body Content: ', req.body)
  const response = createResponse('CCD000201')
  response.header.sessionID = req.body.header.sessionID

  res.json({})
})

// 取得APP版本/強制更版資訊
router.post('/02/ccd000202', (req, res, next) => {
  const response = createResponse('CCD000202')
  response.header.sessionID = req.body.header.sessionID

  let appUrl = 'http://www.google.com'
  if (req.body.header.deviceType === 'iOS') appUrl = 'https://itunes.apple.com/tw/app/dr-parking-4/id1021117809?mt=8&v0=WWW-GCTW-ITSTOP100-FREEAPPS&l=zh&ign-mpt=uo%3D4'
  else if (req.body.header.deviceType === 'Android') appUrl = 'https://play.google.com/store/apps/details?id=com.wooga.sumikko_jp&hl=zh_TW'

  response.body = {
    appDesc: 'Web force to use mobile applications',
    appVersion: '1.6',
    forceAppVersion: '1.6',
    appUrl
  }

  // response.header.returnCode = 'M9999'

  res.json(response)

  const abc = "abc";
})

module.exports = router
