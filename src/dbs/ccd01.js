const express = require('express')
const router = express.Router()
const createResponse = require('../dbs-lib/common-response').createResponse
const faker = require('faker')
const moment = require('moment')
const RandomArray = require('../dbs-lib/FnRandomArray.js')

const userTypeArray = ['1', '2'] // 1.ANZ用戶+卡友,2.卡友
const channelsAllowedArray = ['0', '1', '2']
const yesNoArray = ['Y', 'N']
const cards = [
  {ccNO: '6874634743668139', cardName: '飛行卡', ccFlag: 'M', ccLogo: '110', groupSeq: 0},
  {ccNO: '5915310333250090', cardName: '飛行卡', ccFlag: 'S', ccLogo: '110', groupSeq: 0},
  {ccNO: '2434624722460285', cardName: '豐盛御璽卡', ccFlag: 'M', ccLogo: '150', groupSeq: 1},
  {ccNO: '8959742627339438', cardName: '豐盛御璽卡', ccFlag: 'S', ccLogo: '150', groupSeq: 1},
  {ccNO: '1234567890123456', cardName: '測試卡', ccFlag: '', ccLogo: '160', groupSeq: 2}
]

// 卡片block code
const ccBlockStatusArray = ['', 'B', 'C', 'E', 'F', 'G', 'I', 'J', 'K', 'N', 'O', 'S', 'T', 'X']

// 線上註冊-身分判別
router.post('/01/ccd010101', (req, res, next) => {
  const response = createResponse('CCD010101')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    userType: RandomArray.get('1'), // 1.ANZ用戶+卡友,2.卡友
    showESTMT: 'Y',
    extension: 'phoneNumberKey'
  }
  response.body = data

  // Test for the 廣告
  // response.header.returnCode = 'M9801-S001-1'
  // response.header.returnMessage = '尚未申請信用卡，歡迎立即申辦'
  // response.header.returnCode = 'M0039'
  // response.header.returnMessage = '尚未申請信用卡，歡迎立即申辦'
  // response.header.returnCode = 'M0025'
  // response.header.returnMessage = '已是信用卡/網銀會員，請直接登入'
  // response.header.returnCode = 'M0101'
  // response.header.returnMessage = '您目前無法使用信用卡理財服務，請與本行客服聯繫。使用者資格不符合'
  // response.header.returnCode = 'M0029'
  // response.header.returnMessage = '您目前無法使用信用卡理財服務，請與本行客服聯繫。無資格，不可註冊'

  res.json(response)
})

// 線上註冊-ANZ用戶使用原有代號驗證
router.post('/01/ccd010102', (req, res, next) => {
  const response = createResponse('CCD010102')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 線上註冊-信用卡及用戶資料驗證
router.post('/01/ccd010103', (req, res, next) => {
  const response = createResponse('CCD010103')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 線上註冊-驗證帳號
router.post('/01/ccd010104', (req, res, next) => {
  const response = createResponse('CCD010104')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 線上註冊-建立CCDS會員
router.post('/01/ccd010106', (req, res, next) => {
  const response = createResponse('CCD010106')
  // if (req.body.body.nID === '000') {
  //   response.header.returnCode = 'other'
  //   response.header.returnMessage = '無效的身分證字號'
  // }
  response.header.sessionID = req.body.header.sessionID
  // response.header.returnCode = 'M9701-1101'
  // response.header.returnCode = 'M9701-7788'
  // response.header.returnMessage = 'Oster Test'
  res.json(response)
})

// 開卡線上註冊-建立Card+會員
router.post('/01/ccd010107', (req, res, next) => {
  const response = createResponse('CCD010107')
  response.header.sessionID = req.body.header.sessionID
  // response.header.returnCode = 'M9701-1101'
  // response.header.returnMessage = 'Oster Test(Error code here)'

  res.json(response)
})

// 取得TransitID
router.post('/02/ccd010201', (req, res, next) => {
  const response = createResponse('CCD010201')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    transitID: faker.internet.password(10), // SSO跨通路代碼
    userCode: faker.lorem.word() // 使用者代碼
  }
  response.body = data
  res.json(response)
})

const isVirtualCardArray = ['Y', 'N']
// 驗證TransitID
router.post('/02/ccd010202', (req, res, next) => {
  const response = createResponse('CCD010202')
  response.header.sessionID = req.body.header.sessionID
  const ccList = cards.map((card, index) => ({
    cardName: card.cardName, // 卡片名稱
    ccNO: card.ccNO, // 正卡卡號
    ccBrand: 'VISA', // 信用卡品牌
    ccLogo: card.ccLogo, // 信用卡的產品代碼(系統用來辨別哪一張卡片，與PWeb串接會用此代碼)
    ccDesc: 'VISA Desc', // faker.lorem.sentence(), // 卡片介紹
    isVirtualCard: RandomArray.get(isVirtualCardArray), // 是否為虛擬卡
    expDate: '0221', // 信用卡效期mmyy
    ccFlag: card.ccFlag, // M：主卡, S：附卡
    ccStatus: faker.random.number({'min': 0, 'max': 2}).toString(), // 卡片狀態 (0&2需再參照ccBlockStatus)
    ccID: (index + 1).toString(),
    ccBlockStatus: RandomArray.get(ccBlockStatusArray),  // 卡片block code
    groupSeq: card.groupSeq
  }))
  const data = {
    channelsAllowed: RandomArray.get(channelsAllowedArray),
    // userType: RandomArray.get(['A', 'B', 'C']),
    userType: RandomArray.get('B'),
    ccList,
    dcFlag: RandomArray.get(['1', '2', '3']),
    fpcFlag: 'N', // RandomArray.get(yesNoArray),
    rmsFlag: 'N',
    fpcMessage: '「強制使用者更改密碼(M0031)」',
    rmsMessage: '「請您更新電子郵件信箱(M0038)」',
    pCodelastUpdt: '20170101120000'  // 密碼上次變更時間
  }
  response.body = data
  res.json(response)
})

// 註冊SSO IB用戶
router.post('/02/ccd010203', (req, res, next) => {
  const response = createResponse('CCD010203')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 查詢使用者類型
router.post('/02/ccd010204', (req, res, next) => {
  const response = createResponse('CCD010204')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    // userType: RandomArray.get(['A', 'B', 'C'])
    userType: RandomArray.get('B')
  }
  response.body = data
  res.json(response)
})

// 登入
router.post('/03/CCD010301', (req, res, next) => {
  const response = createResponse('CCD010301')
  const ccList = cards.map((card, index) => ({
    cardName: card.cardName, // 卡片名稱
    ccNO: card.ccNO, // 正卡卡號
    ccBrand: 'VISA', // 信用卡品牌
    ccLogo: card.ccLogo, // 信用卡的產品代碼(系統用來辨別哪一張卡片，與PWeb串接會用此代碼)
    ccDesc: 'VISA　Desc', // 卡片介紹
    isVirtualCard: RandomArray.get(isVirtualCardArray), // 是否為虛擬卡
    expDate: '0221', // 信用卡效期mmyy
    ccFlag: card.ccFlag,
    ccStatus: '3', // faker.random.number({'min': 0, 'max': 2}).toString(), // 卡片狀態 (0&2需再參照ccBlockStatus)
    ccID: (index + 1).toString(),
    ccBlockStatus: '', // RandomArray.get(ccBlockStatusArray),  // 卡片block code
    groupSeq: card.groupSeq
  }))

  // Copy from ccd020501
  const landingData = {
    emailDetl: {
      emailID: 'PER',
      email: null // faker.lorem.word() + faker.internet.email()
    },
    phoneDetl: {
      phoneID: '01',
      phoneCtryCode: null, // '65',
      phoneNumber: '+886987***321', // faker.phone.phoneNumber().replace(/-/g, ''),
      updatedPhoneCtryCode: '+886',
      updatedPhoneNumber: '0987***321'
    },
    addressDetl: {
      addressID: 'HOM',
      zipCode: faker.address.zipCode(),
      address: '台北市內湖區瑞光路583巷25號1樓',
      updatedZipCode: faker.address.zipCode(),
      updatedAddress: '台北市內湖區瑞光路399號'
    },
    blockCode: 'blockCode',
    statmFlag: RandomArray.get(yesNoArray),
    userCode: faker.lorem.word().toUpperCase(), // 'A123456789' 與帳號同
    // userCode: 'A123456789', // faker.lorem.word(), // 'A123456789' 與帳號同
    nID: 'A123456789', // 'A' + '1' + faker.random.number({'min': 10000000, 'max': 99999999}),
    nickname: null,
    fpcFlag: 'N', // RandomArray.get(yesNoArray),
    rmsFlag: 'N', // RandomArray.get(yesNoArray),
    fpcMessage: '「強制使用者更改密碼(M0031)」',
    rmsMessage: '「請您更新電子郵件信箱(M0038)」',
    userType: 'C', // RandomArray.get(['A', 'B', 'C'])
    ccList,
    amtCurrPayment: '12345',
    unbillStart: '27', // 未出帳起日DD
    ccLimit: 20 * 10000, // 信用額度
    unbillAmt: faker.finance.amount(1000, 100000, 0), // 未出帳金額
    avlBalance: faker.finance.amount(1000, 100000, 0), // available balance可用餘額
    rPoints: faker.finance.amount(0, 10000, 0).toString(), // 紅利點數
    mPoints: faker.finance.amount(0, 10000, 0).toString(), // 飛行積金
    crPoints: faker.finance.amount(0, 10000, 0).toString() // 現金積點
  }

  response.body = {
    channelsAllowed: RandomArray.get(channelsAllowedArray),
    userType: RandomArray.get(channelsAllowedArray),
    ccList,
    dcFlag: RandomArray.get(['1', '2', '3']),
    fpcFlag: 'N', // RandomArray.get(yesNoArray),
    rmsFlag: 'N',
    pCodelastUpdt: '20170101120000',  // 密碼上次變更時間
    lastLoginThreshold: RandomArray.get(yesNoArray), // 超過12個月未登入(Y/N)
    ...landingData
  }

  if (req.body.body.userCode === 'THINKPOWER' ||
    req.body.body.userCode === 'EVALIN' ||
    req.body.body.userCode === 'JEANHU' ||
    req.body.body.userCode === 'A123456789') {
    response.header.returnCode = 'M0000'
    response.header.returnMessage = '登入成功'
    response.header.sessionID = faker.internet.password(20)
  } else {
    response.header.returnCode = '0001'
    response.header.returnMessage = '登入失敗'
  }

  setTimeout(() => res.json(response), 3000)
})

// 取得SSO公鑰
router.post('/03/ccd010302', (req, res, next) => {
  const response = createResponse('CCD010302')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    pKey: faker.internet.password(512), // SSO相關功能使用
    rKey: faker.internet.password(16) // SSO相關功能使用
  }
  response.body = data
  res.json(response)
})

router.post('/03/ccd010303', (req, res, next) => {
  const response = createResponse('CCD010303')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    pKey: faker.internet.password(512), // SSO相關功能使用
    rKey: faker.internet.password(16) // SSO相關功能使用
  }
  response.body = data
  res.json(response)
})

// 忘記使用者帳號-身分驗證-查詢使用者帳號
router.post('/05/ccd010501', (req, res, next) => {
  const response = createResponse('CCD010501')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 忘記使用者帳號-身分驗證-查詢使用者帳號
router.post('/05/ccd010502', (req, res, next) => {
  const response = createResponse('CCD010502')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    userCode: 'THINKPOWER' // 使用者登入帳號
  }
  response.body = data
  res.json(response)
})

// 忘記密碼-身份驗證
router.post('/06/ccd010601', (req, res, next) => {
  const response = createResponse('CCD010601')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 忘記密碼-重設密碼
router.post('/06/ccd010602', (req, res, next) => {
  const response = createResponse('CCD010602')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 驗證變更帳號
router.post('/06/ccd010603', (req, res, next) => {
  const response = createResponse('CCD010603')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 變更使用者帳號
router.post('/06/ccd010604', (req, res, next) => {
  const response = createResponse('CCD010604')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 驗證使用者密碼-變更使用者密碼
router.post('/06/ccd010605', (req, res, next) => {
  const response = createResponse('CCD010605')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 變更使用者密碼
router.post('/06/ccd010606', (req, res, next) => {
  const response = createResponse('CCD010606')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 登出
router.post('/07/ccd010701', (req, res, next) => {
  const response = createResponse('CCD0010701')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    loginTime: moment().subtract(20, 'minutes').format('YYYYMMDDHHmmss'), // 登入時間YYYYMMDDHHmmss
    logoutTime: moment().format('YYYYMMDDHHmmss'), // 登出時間YYYYMMDDHHmmss
    stayTime: '00:20' // 停留時間HH:MM
  }
  // response.header.returnCode = 'M9999'
  response.body = data
  res.json(response)
})

// 繼續使用系統
router.post('/07/ccd010702', (req, res, next) => {
  const response = createResponse('CCD010702')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

module.exports = router
