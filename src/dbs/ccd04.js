const express = require('express')
const router = express.Router()
const faker = require('faker')
const fs = require('fs')
const path = require('path')
const RandomArray = require('../dbs-lib/FnRandomArray.js')
const createResponse = require('../dbs-lib/common-response').createResponse

// AD Image/Function Link 廣告與功能連結
router.post('/01/ccd040101', (req, res, next) => {
  const response = createResponse('CCD040101')
  response.header.sessionID = req.body.header.sessionID
  let resData = {}
  let randomKey = RandomArray.get([0, 1])
  const { location } = req.body.body
  let imgSource = '../assets/loveEarth.png'
  if (location === '9' || location === '10') {
    imgSource = '../assets/banner_380_110.png'
    randomKey = 0
  } else if (location === '11' || location === '7') {
    imgSource = '../assets/loveEarth.png'
    randomKey = 0
  }

  if (randomKey === 0) {
    fs.readFile(path.resolve(__dirname, imgSource), function (err, data) {
      resData = {
        ADId: faker.random.number({'min': 10000, 'max': 1000000}).toString(),
        ADType: '0',
        ADImgType: '0',
        ADImg: (err) ? '' : `data:image/png;base64,${new Buffer(data).toString('base64')}`,
        ADLinkURL: 'https://www.dbs.com.tw/',
        ADImgURL: '',
        ADLinkFunc1: {},
        ADLinkFunc2: {},
        ADLinkFunc3: {},
        ADLogo: location === '7' ? '170' : ''
      }
      response.body = resData
      res.json(response)
    })
  } else {
    resData = {
      ADId: faker.random.number({'min': 10000, 'max': 1000000}).toString(),
      ADType: '1',
      ADImgType: '',
      ADImg: '',
      ADLinkURL: '',
      ADImgURL: '',
      ADLinkFunc1: {
        funcParent: '',
        funcCode: 'ccd020000',
        funcName: '回首頁'
      },
      ADLinkFunc2: {
        funcParent: 'ccd000001',
        funcCode: 'ccd030100',
        funcName: '本期帳單'
      },
      ADLinkFunc3: {
        funcParent: 'ccd000002',
        funcCode: 'ccd050100',
        funcName: '更新個人資料'
      },
      ADLogo: ''
    }

    response.body = resData
    res.json(response)
  }
})

// Image Switch 背景圖片
router.post('/01/ccd040102', (req, res, next) => {
  const response = createResponse('CCD040102')
  const { switchPara } = req.body.body
  const bgs = [
    'bg_login.jpg',
    'pre_login_afternoon.jpg',
    'pre_login_dawn_beach.jpg',
    'pre_login_morning_food.jpg',
    'pre_login_night_party.jpg',
    'pre_login_noon_balloon.jpg',
    'pre-login_4_painting.jpg'
  ]
  const imageFileName = RandomArray.get(bgs)
  let imgSource = '../assets/' + imageFileName
  if (switchPara === 'S01') {
    imgSource = '../assets/' + imageFileName
  } else if (switchPara === 'S03') {
    imgSource = '../assets/home_banner.jpg'
  }
  response.header.sessionID = req.body.header.sessionID
  fs.readFile(path.resolve(__dirname, imgSource), function (err, data) {
    const resData = {
      switchDesc: faker.lorem.words(5),
      pWebURL: (err) ? '' : `data:image/jpeg;base64,${new Buffer(data).toString('base64')}`
    }

    response.body = resData
    res.json(response)
  })
})

// APP行銷通知
router.post('/01/ccd040103', (req, res, next) => {
  const response = createResponse('CCD040103')
  response.header.sessionID = req.body.header.sessionID

  response.body = {
    notificationType: '0',
    appLocation: '0',
    notificationSubject: '這是行銷通知主題',
    notificationContent: '這是行銷通知內容',
    notificationURL: 'http://dbs.promition.url.sample'
  }

  res.json(response)

})

// 上傳個人圖片
router.post('/04/ccd040401', (req, res, next) => {
  const response = createResponse('CCD040401')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 下載個人圖片
router.post('/04/ccd040402', (req, res, next) => {
  const response = createResponse('CCD040402')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    pic: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTEhMWFRUVFxYVFxgXFRUVFRcWFhYWFxYVFxcYHyggGBolGxcVITEhJSkrLi4uFyA1ODMtNygtLisBCgoKDg0OGhAQFy0dHR0rLystLS0rKystLS0tLS0tLS0tLS0uKystLS0tLS0rLS0tLS0tLS0tKy0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABgcEBQgDAgH/xABJEAABAwIDBAYGBQgJBAMAAAABAAIDBBEFEiEGBzFBEyJRYXGBFDJCkaGxIzNSssEIFWJygpKi0SQ1Q1Njs8Lw8SU0c6NUdIT/xAAYAQEBAQEBAAAAAAAAAAAAAAAAAQIDBP/EACIRAQEAAgIBBQADAAAAAAAAAAABAhEDMUEEEiEiURMyYf/aAAwDAQACEQMRAD8AvFERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQERavaDaCmoojLUytjbwF9XOP2WNGrj3BBtF41VVHG3NI9rGjm5waPeVBG4njGIf9rG3DqY8JqhvSVLxycyHgz9r3rJpt2NIXCSsfNXS8c1RI4tB/RjbZoHdqg98Q3oYTEbGra89kTXy/FgI+KwH72aT+zpq6QdrKY2+JCmWH4NTQC0MEUQ/QjY35BZyCtKjfPRx/W0tbH+vC1vzesvD98mESEAzPiJ/vInAeZbcD3qfSRhws4Ag8iAR7iobtFuuwurBJpxC8+3BaN1+3KBlPmEEmwrGaepbmp5o5W9rHtd77cFnLm7ardVX4aTUUcr5mNuc0OaOojHaWtNyOGrT5Be+yG+urgsytb6TF9sWbO3/S/zse9B0Ui1Gze0lLXRdLSyiRvMcHsP2XtOrT/sLboCIiAiIgIiICIiAiIgIiICIiAiIgIi0O1O0ApwIYix9ZM1/o8LnW6RzRfXsA142vayD42r2l9GbkhjNRVPA6OBpGY3OUPdr1YweJ/5Wt2b2Md0orcSeKmtPC/1NOD7EDDoLfa4/G/rshsyKNslVVyCSrlGaedxAa0cejYT6sbfwX5ge3sNbVmno2mVjMxklJLGho0DowR1wXaX080EvREQEREBfl141NWxnrHjwHNRja3bmKibE7I6TpJMpA0cGhpc4jtNhwQk38Jcqm3obqmVAdVUDAyoF3PiGjJuZsODZPgfHVWFhm0lNPG2WKS7Hi7TY/7BW0jla4XaQR3IOP8AA8TqaOYTUr3RysJa5pHZxjkYeI7j810fu33gQ4nEQQI6hgHSR34/ps7W/JRLfTsLcOxGlZ9I0f0hjfbYP7UAe03n3a8lU2HVU0D21tI7LLDZ7hyLTxcRzYeDhyv3qdK66RaTY7aOLEKSOpi0zCz282SD1mHwPvBB5rdqoIiICIiAiIgIiICIiAiIgIi/HOsLnQBBp9rdooaCmfUzHRujWj1nvPqsb3n4C55KM7FYK+PpcWxNwFVM0uObRtLABcRNv6unH3cbk67Bm/nrETVv1oKFxZTNPqzTi2aYjmBpb9nvXrvq9IdFCwRyvpC4uqehbmectujY4DUMvck9wRZN3SMbY7VfnKJ7/pI6OJxLIr5TW29Uu5gXBs3mNeIU+3W7MGjpekmaBU1FpJdAMgt9HCAOAY3l2kqB7C4WMSrY5chFHRWfZzS0PqPYZlI4NGv/ACrwWcd9115fbL7MPmTz+0REWnEREQRrGielN+wW8LKt95c30lFHzzyyeTY8v+pXFX0LZBroRwP8+5UftfmlxOQDrNpWNhuNR0j+u/3DKFM79a7emx93LjP9fmwFeYKt9IT9FOHSxDk2Rv1jR3Ea+Ss2CZzDdpsf98VUFOwitonNHWFQG99nNcHfAK44aKR3Bp8ToPeU4rvFr1eEw5bIkFDUiVmoH2XDiP8AgrmzbXA34XiT2RaRu+mguLtyOJzREc2jVpHYuk8No+jba9ydSq2/KBwsOo4aoDrU8oBP+HL1XD94MSvPEf3WYm2lqQ+LShrXCN7CbmlrPZY7sa7UNdzzN7Nb0XJ+AY6aObO5ueCWzJ4/tNBuHt7Ht4g9y6kwiuZPDHLG8PY9oLXDmLcfFJdxazERFUEREBERAREQEREBERAUA3rYxLkhw2lP9Jr3dHcexD/avPlceGbsU9keGgkmwAJJPAAcSq13bxmvravF5B1S401IDyhZ6zh4n4lyCFbcbeT4Y8YVh2WGOlYxjpMrXSPe5oe5wvoNXG+lybqJU29PGGOv6Y53c5kbgf4Vbe9ndpDWH0uKWOnnNmvMhyxS2Fm5ney+wAvrewVTT7qcXaerTCRvJ0csLmnvHWv8FZoTzZjfkTlZVwMabjNJGbAtOjnZDwcONr6gHhorvY4EAjUHULleg3V4m6SNj6cx53WJJBDG6ZnuLSQAATpe5twXU0EYa1rRwaAB4AWCg+0REBF41dUyJjpJHtYxou5ziGtA7SSqr2n35UkJLKOM1LhpnJ6OLy0zO9wQWy69tOPLsVQQ7C4rGX5W0she98j3maRpe97i4m3R6dluwKB4nvoxaW+R8UA/w4mk28ZMy0ku8fFnca6byIb90BLjvtvj5MuO7xul3bP7FVgqYJ6n0djYHmTLG98jnHo3sAu5rQBd9/JWSuS6feXi7OFbKf1sj/vNKlezu/OtjcBVxsnj5lrRHL4i3VPhYeKTHXRnyZZ3eV3XRKi29CkEuFVjSL2hc/zj+kH3VvMGxSKqgjqIXZo5WhzTwNuwjkQbgjuXltLFmpKlp9qCYe+NwRhyW4Zo/IH3K2/ydMdJbUUTjoy00YJ4B3VkAHZfKf2iqkoT9G3w/FSHdPiHo+MU+tmyF0LvCQHL/FlXPHvTVdSoiLoyIiICIiAiIgKuNr96rKGvbRugLmjo+lkzhuUSWILW2OawOtyFnbU70aOhqxSzNlJAaXvY0FseYXbcXudNdBzVS755Ypa9tRC9skVTSte17TcEtL2nwOg04qUZe8TeNWtxKVtNO9kUDg1rWkZH5QC4u061zfyU12r3xxUk0MbIOmDoo5ZSHhpYJWhzWtFjd2U31txCoQVOZji43e7MShkMjST1pHZWjt5MaB5ABTauj96O0JGFt6C/SV/Rwwjg76YXPgctx4kKWbM4O2kpYaZnCJjW37XW6zvN1z5qvnUnT41Q0nGPDKVsr+zpS1rWg9/1Z8irUWkVrvHxyo9J9Ep5WsDYBJIHxMlY/pHuaGOa4cLMPD7SjG73ZmOtkqWSxupzAWgy0c0sMbnu1LOjN2ggWJy9q89sq6R2K1oiGaV7qWkgb2vMWYk9wLyT4K3dlMBZRUsdOzXKLvdzfI7V7z3k3+CzN7v475zCceOp9r2+MC2ZipfVknkPbNUSy+4OOUe5btEWnAREQRvbTBqKeIOry7omezneGlxOnUZ6zuziVX9RsxhTh/R8FrZh9q0kDffK9p+Cs3H9o6WjANRIGud6jAC+V57GRtu53kFoHY/itR/2dA2Bh4SVr8hPf0Ed3e8hBXlRu/jd6mBVTP8A98V/c4lYMW6Qym3o9dTX4OeaWoYPHI9rreStA7OYvLrNiwiH2aemY0fvvJcozivoUDi2bH66SQaFkMoe4HvbEw280EPxLcViDNYZYZh3l0Tvc4EfFbHd1u1pXSyU+JxTNqmt6RsZeGxPiuW52Fhu+x468xopNs9VU00rYosVxRj3khgnFmvIBJDTJFYmwJt3Ld7U7I1j+iqaaqzVdLcxGRjG9I11s8MhZYFrgOzzF7ptbLO2NUbJV2Hx5sIqHPjZd3oc9nscC7M4Rv0c1xueJ1J4rZYLtfHiGH1MgaYpYo5WTRO9aOQMdcd40Nj3HmFgxb1aWIZMRjloqgDrRvikeCe2N7AQ5vfotDRRP9FxzFDG+FlXG/oWPGVxayJ7RK5vLM510RS+H/Vt8/mV8MqOiqYpR/ZyRyfuuB/BetGOo3wCwsVGvl+K5T+zV6dnxvBAI4EA+9fS12zk2ekp3/ahiPvY0rYrqyIiICIiAiIgrHfJsIatgrKZt6iEWcwf20Qubfrt1I7dR2LnpzC8kC4a25DTc2J4gDku01y7vZjEWK1TY2hjS+N5DRYEviYXG3ecx8VmrEKZTE8A75LNp6gU80MrWB5jc2XK4ktJYQWg2N8twvSoGZoI1HG3b2BfFJHpbJdzr2a0EuIAudOwC58lmVdLV3NbXGfFKrp4x01aA8PaTlaIgfow062tzv7IV6KqtymxLIYmYi85pZ4/o2gWbFG4/wATjYXPkrVXRlW2w+yz3YnXYhUNItUTspw4H7WR82v6LWtB7M3crJREBERAWDjBn6MinyiRxDQ5+rIweMhb7VhwbzNuV1nLX45QOnhdCHlgks17mkh3R364aRwJF235XugiOztCHSvNHd2pbPiE1pJpng9ZlPcWyg3Fx1BwANtJxT0zWDS5PNziXOPiSlLTMjY2ONoYxgDWtAsGtAsAB2LWYnivsxnxd+A/mg9sXqosjo3jPmBaW8rHtPJUjilPJSTej3tG4F8LgAC5gOrHEcXtuNeYsVZ5Khu9GEeixze1DPGQe55yOHmCpnhLHb0/NePOWdeULxmsdE1k7XEOhlilabm4LXgfIldCYRinSAB3rEXHYRb5rnLHmGXoqZur55GNA7ri58Fd8ByZbeza3ks8M+rv6+z+X4/EvfG02uAbcLgG3goJvxruiwiYA6yujiHfd4c4futKnkb7gHtAPvVL/lD4jd1HSj7T53d1hlZ85PctvEqmNtgB2ABa7FOPl+K2a1OIG7j5Bcse26652J/q+jv/APHh/wAtq3aw8GpxHTwxjgyONv7rQFmLqwIiICIiAiIgKkvygoIBNSODfp3iQPcOcTALBw59Y6HuKu1c477cQ6TFntvcU8DGW7HOHSH749yl6IhccrTex4K6tkMEp6fAJqsRjp5qOd8kh1ebsks1pPqttbQKg4jYPP6J+Oi6Mx1hg2YLeYooWnxe1jT94rOMWpHu2/qqi/8ArxfdCkqiO6acPwijIPCPL5se5h+S3e02JGmpKioa3O6GKSQN5EtaSAbctFtGzRc7YJv0rWSXqoo5ojxawdG9v6puQfA+9WPh+9jDKllhUmmfobTNLfLMLt+KCwl8veBqSAO82UNO1sGW4xClLftdPF+Oqj+J7xcMj+sqzMfswtfIT3ZjZo96uhPNpsaFNBnbZz3uZFC37csjg1g8Lm57gVtKdha0AnMQBc9p5lUpsxta7GMXpY2x9HTUgknYwkF7nBuUPeeF7uFgOCt3GqrI3KOLvgOagxMVxK92MOnM9vcO5alEW0FD96klqHLYkvmhaAASTZ2awA4nqqYIR8NUqxA9g9lZGyGtq25ZSLRR/wB0w8z+kR7rntU8RZmFU2eQdjdT5cB71JNRcsrld1I6dtmtHYAPguYN4OL+l4pUyg3ZGegj/Vj0J83Zj5q+95O0IocPmmBtIR0cXaZH6N92rv2VzHSx5WgHjxPiVzzvwR7LVwysEzHP9QSNLranKHC9hz0WTiJNgO028dCta6PUaWv/ADWcItdWbE7w6PEnPjgzskYM2SRoa5zL2ztsSCOHfqFLgVx/geIzUUxmifkf0UzWuHEZmFoPjcg+SkO6HE5xi9OOleemMjZQXOdnHRvcc1+JBAN+5blZdQIvKnqWPvke11jY5XB1j2G3Ar1VBERAREQFyPtjX9NW103HPUPa39Vji1v8IC61mJym3Gxt420XGWUmK545i53be5Bus5LH4yIuYQOLnNaPMhdLb3Y8mB1DR7LIG+6aILmyjf7NyDcFpGhDhqCD26BWlLt8a7B6uiqyBVMiD2P0DZ2RPY8nukAabjnxHOzEqY/k/wBeJML6PnDNIzydaQffKsiaIOaWuF2uBaR2gixCoT8nLFslRUUpOksbZWD9KM2d52d/Cr+Wkci7w9kZMNq3QuBMTruhfyfHfhf7TeBHnwIUYXZG1Gz1NXwmnqWhwPWbYgSMI0zsPEEX8NbHiudttd1FdROc+NpqYOIfG0l7R/iRjUeIuPDgrKIAiFFpFvfk3Qg1lS7m2AAftSC/yVw46fpfBo/FU1+TjUgV08fN8Fx+w9t/vK8scpC4B7dSBY+Hb5LM7VoURFpBEX0xhJsBcnkgMYSQALk8FJ8PpBGy3M6k968cMw4RjM7Vx+HcFo95e1gw6jdILGaT6OBvG8hHrEdjRqfIc1m1VS76to/Sq5tLGbxUnrW4OnPH90WHjmUHXnAwgEuN3OJc4nUlx1JJ5r0XDK7rcjBxGTVo7HAleNdOHFtuR/kvKpddzj3r7ma0tDhcX9wK3JrSP2rnDnM5cj4XCz6XEXRyuMJs90b4w4aFvSCz3A8jku2/6S05ae26+m3Fz8ldIs7cDDOMSdkuI+hcZh7PEdHfvzcPNdGKBbmtmDR0IfKLT1NpZLjUNt9Gw+Ddbdrip6tRBERAREQFz5vZ2GdRyvq4RmpZnkyN/uZHm5/YcT5cOxdBrwraRksbopWh7HtLXNIuC0ixBUs2ONKiHKdOB1BXu1wkbYmzhwKlm8TYp+GTZdXUkpPQyHXIePRvPaOXaPNQqRhafkVjTTZbMYs6iq4akcYJBnHbG7Rw77tJ9669pahsjGyMIcx7Q5pHAtcLg+5ccPZnZm9oX8wOSujcLtmHx/m6d3XjBdTk+3HxMfi3Uju8FuVKsLbXApamJr6WXoaqAl8D+V7daN/axwsCNeAPJafZDeEyZ/olc30SuZ1XRv0a8j2onHQ342v4X4qdLQbU7H0eINAqYg5zfVkb1ZG/qvGtu46Ko1G02B0k0xMtLA8j2nRMzHncutdaIbD4ZmzehRX8Zcv7ueylWBbIOpxkdWTTxDRrJhG5zR2CQAOst03CIgb2J7idFfgVji+Hx0GJYfiEMTYad96OcMblYzNcRvsOAJI1P2FbixsQw6KaJ0MrGvjeMrmkaEfgvzDaQxRtjzl4YLNLvXyjgHH2iBpfmoMarwljySw5TzHEX46jksI4LJ2t95/koPtHj9ZhFdVVL4OlpKmSJw69rZIWtOQ65X9U9VwAIAsVZ+F1zJ4Y5mXySsbI2/GzgCL9+quxrocDPtO9w/ErZ0tGyMdUefP3rWQYwY6g01SWNLg+WB4IAkjaRnaQT1XszNvyINxzA0e0m2Do4H1DJI6aBtwyaZjpHTu7IYQ5pLTycTrxtbVTYk+PYzDSQPqKh4ZGwXJ5k8mtHNxOgC5l2s2jlxGqNTKC1ou2GO+kcd/vniT/ACC+NotqKzEXMfVvBbH9XG1uRgP2y2561u8rXLnll4akF+E6L9XxMeqfA/JYaayjaC+x5hfs0hY45CQAdQeHuWTgNA2eqp4HEtEs0cZcOID3BtxfnqpHthsZNh04jmtIx4JilAsHgcQ4cni40710v6w0jQHAEgai6+9nww1dMAL3qYAQdQR0g0tzCxqrP7PDu4qUbodmpKrEYn5foqZ7ZpHHgHN1jZ+sXAadgKmMWuoERF0ZEREBERAREQYWM4VDVQvgnYHxvFnA/Ag8iOII4Lm/brYWbDH9a8tI51o5berfhHL9l3fwPwHTqx8Qoo5o3xSsD43gtc0i4IKlmxyQ1oGg4LDoc7CHxOLJYX3Y4GxBBuNVKNvtmzhdSYXOzQvBfA7i7JexY7vB0vzUboTcF32nE/gufzGu3Q+7LeRFiDBDPaOsYLOZwEluL4/mW8vBWAuPXx6hwJa5pBa5ps5pHAghTzZ7e5iNMA2oa2sjHMnJMB+sBZ3mL963MpU06GRVrhW+vDJLCbpad3Y9hcP3mX+IClFHt1hkvqV1PryMrWn3OsVpEiRa78/Ulr+kwW7eljt81hVm2uGxfWVtOO7pWOPuaSSgqXfzX1D5DE6KVsUeRsZ6N3Rvz9aWXOOrpZjA06+t2qVbqNv6SWCnoHl0VRHEyNoeOrLlaLFjhpcjWxsfFa7b7e9RvpZqehdJLNI0xh7WFrGh2jnXdYnS9rDiqRjgkL+kBMXZlJzDS2hHBS2QXLvK2upY8UBdCys9Hpi1kfVcxtRJKCXPOuXK1jeAv1racVWeN4vU1s3T1b87hoxg0jjb9ljeXz01JWFSMEZuzj2nW/ce0dy2lTQh0fTw6sFhI3iYnHh4sPI+RWLlvpqRrkRFhoXnUeq7wK9F8SjqnwPyQY2AT5KulefYqIXfuyNP4K8Pyg/qaM/4z/jEVz9JwV57564T4dhsw1ErmyfvQX/FdfDHlT9JOXEg+K6T3OBn5ppy1rW36XNYAZnNle0ud2mzRr3LmfDvWPh+K6R3IH/pEH68/wDnSKY9lTxERbQREQEREBERAREQUn+UjRaUc3YZYj5hrm/JyqWhPU8yuht+FDHJhjjJp0csTmu+w4uyZj+j19e5c8UbC3MxwsWn/fksZrGSiudmCxVWzcZaxofFE6RpAF88TnZ9f0g1wPiqYWLNNSvlzAeIB8QvF1FGfZHyWQim10xvQI/s/Er6bRxj2R817rxmqGt7z2BXdR6taBwFl+rXPrHHhYfFfDKlwNySRzF7afzT2022izMJxJ8Ege0Aj1XMdq17D6zHDsK01W2SPKcxcx4zMdycOBHc4HQj+a/I64+0PcntptLcfwZgjbV0t3U0htY6uhk5xP8AwPNaBbfZPaIU7zmAlp5RkniPB7O0Dk4cQVkbV7PCnLJYHdJST9aGTj3mN/Y8fG3ig0C/F+oorSyt4jsW3GKzS08ccs73xxX6ONzrtYQC0ZRy0K1tUOu7x+a+KKEOvd1rLp4YZOHDrHw/FdI7jx/0en73Tn/3yBc7UUQJyRB0r3cGsBe5x7AGrprdbg8tJhlPBO3LI0SOc24OXPK94Btzs4JiVK0RFtBERAREQEREBERBFN6lN0mE1jeyIv8A3CH/AILmDD5s1gfWtZp7R9k+HL3LrvH6XpaaeLjnikZ+8whcawOtY8wfks5dLFn7ObwJKWglo+iD84eI35rZOkBzAi3W1JI8VCrKRbEPoDO78436IxuykZvrLtsTk14ZvOy0E2XM7LfLc5b8ct9L99rLk0+UReNVLlb38AivKrqbdVvHmexYCItyaZERFRucArIiDTVJtBKbh/OCXg2ZvdycOY8Fh4xhklNM6GUWc3mNWuadWvaebSNbrCUwwSZlfA2hmIbURA+hyu0v20rz2H2Ty+dREY3kG4U02R2kY1j6apBfSy/WN4uifymj7COPfZQ2ogcxzmPaWuaS1zToQRoQV+0slnDv0Pms2Kk+PYQ6mkykh7HDPFI31ZIzwcD8xyK1q3OF4i10fotQfoibxv4mCQ+0O1h9oea1lbSuieWPGo7NQQeDgeYI1BWKrT146/iB/JTzcXQU09dNDUwRzAwl7BIxrwCx7b2vw0d8FB8RbwPkpZuTqujxiAf3jZWf+su/0rpizXStBhkEIywwxxDsYxrB/CFloi2giIgIiICIiAiIgIiIPwri6s+tk/8AI/7xRFKNjB6o8B8l9oi4ugsHEeLfNfiKztKxCiItoIERAXvQ/Wx/rs+8ERIVI96H9Zz/ALH3GqLM4jxX6iUbhbTGPq6X/wAI+85EXOK0Nf6o8VvN0/8AXFH+s/8AypERbwSurERF0ZEREBERB//Z',
    picExtension: 'jpg'
  }

  response.body = data
  res.json(response)
})

// 更新個人資料Email
router.post('/04/ccd040403', (req, res, next) => {
  const response = createResponse('CCD040403')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    email: req.body.body.email
  }
  response.body = data
  res.json(response)
})

// 變更手機號碼
router.post('/04/ccd040404', (req, res, next) => {
  const response = createResponse('CCD040404')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0405'
  response.header.returnMessage = '新電話與舊電話相同，資料未更新(M0405)'
  res.json(response)
})

// 變更通訊地址
router.post('/04/ccd040405', (req, res, next) => {
  const response = createResponse('CCD040405')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 取得通知設定
router.post('/04/ccd040406', (req, res, next) => {
  const response = createResponse('CCD040406')
  response.header.sessionID = req.body.header.sessionID

  const data = {
    settingList: [
      {
        nusSerno: 1,
        ccsdSerno: 13,
        groupId: 'A08',
        groupName: '繳款完成通知',
        smsOpen: 'E',
        mailOpen: 'D'
      },
      {
        nusSerno: 2,
        ccsdSerno: 13,
        groupId: 'A14',
        groupName: '分期設定完成通知',
        smsOpen: 'D',
        mailOpen: 'E'
      }
    ]
  }

  response.body = data
  // response.header.returnCode = 'M0003'
  // response.header.returnMessage = '連線逾期 - Mock Server Message'
  res.json(response)
})

// 更新暱稱
router.post('/04/ccd040407', (req, res, next) => {
  const response = createResponse('CCD040407')
  response.header.sessionID = req.body.header.sessionID
  const data = {
    nickname: faker.name.findName()
  }
  response.body = data
  res.json(response)
})

// 變更裝置
router.post('/04/ccd040411', (req, res, next) => {
  const response = createResponse('ccd040411')
  response.header.sessionID = req.body.header.sessionID
  res.json(response)
})

// 變更通知設定
router.post('/04/ccd040412', (req, res, next) => {
  const response = createResponse('CCD040412')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  response.header.returnMessage = '成功變更'
  res.json(response)
})

// 取得信用卡掛失地址
router.post('/05/ccd040501', (req, res, next) => {
  const response = createResponse('CCD040501')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  response.header.returnMessage = '取得資訊成功嘍'

  response.body = {
    // zipCode: '23456',
    zipCode: ' ',
    address: '這裡是地址喔？'
    // address: ''
  }

  res.json(response)
})

// 信用卡掛失/補卡
router.post('/05/ccd040502', (req, res, next) => {
  const response = createResponse('CCD040502')
  response.header.sessionID = req.body.header.sessionID
  response.header.returnCode = 'M0000'
  response.header.returnMessage = '成功變更'
  // response.header.returnCode = 'M200200'
  // response.header.returnMessage = '不成功變成人'

  response.body = {
    zipCode: '23456',
    address: ''
  }

  res.json(response)
})

module.exports = router
