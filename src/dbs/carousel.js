const express = require('express')
const Module = require('module')
const fs = require('fs')
const faker = require('faker')
const router = express.Router()
faker.locale = 'zh_TW'

Module._extensions['.jpg'] = function (module, fn) {
  var base64 = fs.readFileSync(fn).toString('base64')
  module._compile('module.exports="data:image/png;base64,' + base64 + '"', fn)
}

const banners = [require('../assets/homecarousel-1.jpg'),
  require('../assets/homecarousel-2.jpg'),
  require('../assets/homecarousel-3.jpg')]
const mobileBanners = [require('../assets/homecarousel-mobile-1.jpg'),
  require('../assets/homecarousel-mobile-2.jpg'),
  require('../assets/homecarousel-mobile-3.jpg')]

router.get('/carousel', (req, res, next) => {
  const data = {
    rotationTime: '5',
    displayCarouselDots: 'Yes',
    locale: 'zh',
    heroBanners: banners.map((item, index) => {
      return {
        headline: 'fly',
        subheadline: '飛行卡',
        buttonText: 'Learn More',
        buttonURL: 'https://www.dbs.com/',
        buttonURLOpenTarget: '_self',
        navTitle: 'DBSCard',
        navSubTitle: 'CardSubTitle',
        s12Image: item,
        s12ImageAltText: '飛行基金1.5%',
        mobileImage: mobileBanners[index],
        mobileImageAltText: '首刷送500元',
        mobileOverlayImage: '',
        mobileOverlayImageAlt: '首刷送500元',
        mobileOverlayImageLink: '',
        mobileOverlayImageLinkOpenTarget: '_self',
        textColor: 'white-text',
        buttonColor: 'btn-primary',
        buttonTextColor: 'white-text'
      }
    })
  }
  res.json(data)
})

module.exports = router
