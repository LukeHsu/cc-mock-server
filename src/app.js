const express = require('express')
const path = require('path')
// var favicon = require('serve-favicon');
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const time = 0
// -- This is DBS mock router --
const dbsRoutes = require('./dbs/index')
const ccd00 = require('./dbs/ccd00')
const ccd01 = require('./dbs/ccd01')
const ccd02 = require('./dbs/ccd02')
const ccd03 = require('./dbs/ccd03')
const ccd04 = require('./dbs/ccd04')
const ccd05 = require('./dbs/ccd05')
const ccd07 = require('./dbs/ccd07')
const ccd08 = require('./dbs/ccd08')
const cards = require('./dbs/cards')
const carousel = require('./dbs/carousel')
const ib = require('./dbs/ib')

const app = express()

// CORS All Request
const allowCrossDomain = (req, res, next) => {
  // let allowedOrigins = ['http://localhost:3000', 'http://0.0.0.0:3000']
  // let origin = req.headers.origin
  // if (allowedOrigins.includes(origin)) {
  //   res.header('Access-Control-Allow-Origin', origin) // restrict it to the required domain
  // }

  // res.header('Access-Control-Allow-Origin', 'http://localhost:3000; http://0.0.0.0:3000;')
  res.header('Access-Control-Allow-Origin', req.headers.origin)
  res.header('Access-Control-Allow-Credentials', 'true')
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header('Access-Control-Allow-Headers', 'Content-Type, pweb-access')
  res.cookie('isVisit', 1, {maxAge: 60 * 1000})
  next()
}

// 模擬rest api timeout(秒)
function delayServer (callback, time) {
  setTimeout(callback, time * 1000)
}
// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'))
// app.use(bodyParser.json())
app.use(bodyParser.json({limit: 1024 * 1024 * 5, type: 'application/json'}))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(allowCrossDomain)
app.use((req, res, next) => { delayServer(() => next(), time) })
// app.use(bodyParser.json({limit: '50mb'}))
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/', dbsRoutes)
app.use('/api/ccd00', ccd00)
app.use('/api/ccd01', ccd01)
app.use('/api/ccd02', ccd02)
app.use('/api/ccd03', ccd03)
app.use('/api/ccd04', ccd04)
app.use('/api/ccd05', ccd05)
app.use('/api/ccd07', ccd07)
app.use('/api/ccd08', ccd08)
app.use('/api', cards)
app.use('/api', carousel)
app.use('/ib', ib)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Page Not Found')
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    console.error('Something wrong.', err)

    res.status(err.status || 500).json({
      code: err.status,
      message: err.message,
      error: JSON.stringify(err.stack)
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  console.error('Something wrong.', err)

  res.status(err.status || 500).json({
    code: err.status,
    message: err.message
  })
})

app.rootPath = __dirname

module.exports = app
