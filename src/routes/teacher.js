var express = require('express');
var router = express.Router();

const teacherList = [];

router.post('/create', function(req, res, next) {
  console.log('Create a teacher.', req.headers, req.body);

  var newTeacher = Object.assign(
    {}, 
    req.body.teacher,
    {id: Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000,}
  );
  teacherList.push(newTeacher);

  // console.log('After add to list: ', teacherList);
  setTimeout( () => {
    res.json({
      result: 'success',
      message: 'This is create teacher response.'
    })},
  1000);

  // res.json({
  //   result: 'success',
  //   message: 'This is create teacher response.'
  // });

  // res.status(500).json({
  //   message: 'This is error test.'
  // });
});

router.post('/list', function(req, res, next) {
  console.log('List teacher.', req.headers, req.body);

  res.json({
    result: 'success',
    teachers: teacherList
  });
});

router.get('/oster/200', function(req, res, next) {
  res.json({
    result: 'success',
    message: 'This is status 200 response'
  });
});

router.get('/oster/204', function(req, res, next) {
  res.status(204).json({
    result: 'success',
    message: 'No thing special but status code.'
  });
}); 

router.get('/oster/bad', function(req, res, next) {
  res.statusMessage = "I love Customized error code without Http BODY";
  res.status(407).end();
}); 

router.get('/oster/500', function(req, res, next) {
  res.status(500).send('I love 500 with no Http BODY');
}); 

router.get('/oster/badGuy', function(req, res, next) {
  res.json({
    OsterBadGuyResponse: {
      header: {
        what: 'This is not HTTP Header'
      },
      body: {
        what: 'This is not HTTP Body'
      }
    }
  })
}); 

module.exports = router;