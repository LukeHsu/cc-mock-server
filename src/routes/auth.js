const express = require('express');

const router = express.Router();
const actionLogin = (req, res) => {
    console.log('Login >> ', req.headers, req.body);
    const userId = req.body.userId;
    const enterKey = req.body.enterKey;

    if (userId === 'thinkpower' && enterKey === '111111') {
        res.json({
            rtnCode: 1,
            rtnMessage: 'login success',
            userInfo: {
                token: 'demo token',
                userId,
                userName: 'Demo A',
                isAuthenticated: true,
                cards: ['1234567812345678'],
            },
        });
    }

    res.json({
        rtnCode: -1,
        rtnMessage: 'login failure',
        userInfo: {},
    });
};
const actionLogout = (req, res) => {
    console.log('Logout >> ', req.headers, req.body);
    const id = req.body.userId;
    console.log(id, ' >> logout!');

    res.json({
        rtnCode: 1,
        rtnMessage: 'logout success',
    });
};

router.post('/login', actionLogin);
router.post('/logout', actionLogout);

module.exports = router;
